<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientTransaction extends Model
{
    protected $guarded = ['TransactionID'];

    public function carrier()
    {
        return $this->hasOne(Carrier::class, 'CarrierID', 'CarrierID');
    }
}
