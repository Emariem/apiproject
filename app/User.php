<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Acme\Common\DataFields\User as DataField;
use Acme\Common\DataFields\ClientAllocation as ClientAllocationDataField;
use Acme\Common\Constants as Constants;
use App\UserInfo;
use App\ClientAllocation;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'is_active', 'type', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    public function info()
    {
        return $this->hasOne(UserInfo::class, 'UserID', 'id');
    }

    public function activeAllocation()
    {
        return $this->hasOne(ClientAllocation::class, 'ClientID', 'id');
    }

    public function ClientAllocation()
    {
        return $this->hasMany(ClientAllocation::class, 'ClientID', 'id');
    }

    public function validateForPassportPasswordGrant($password)
    {
        //check for password
        if (Hash::check($password, $this->getAuthPassword())) { 
            //is user active?
            if ($this->is_active) { 
                return true;
            }
        }
    }
    
}
