<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Acme\Common\CommonFunction;
use Acme\Repositories\GatewayRepository as Repository;
use Acme\Repositories\CarrierRepository as Carrier;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\Gateway as DataField;
use Acme\Common\Constants as Constants;

class GatewayController extends Controller
{
    use CommonFunction;

    protected $repository;
    protected $carrier;
    protected $result;
    public function __construct(Repository $repository, Carrier $carrier, DataResult $result)
    {
        // set the model
        $this->repository = $repository;
        $this->carrier = $carrier;
        $this->result = $result;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exclude = [Constants::SYSTEM];
        $options['carriers'] = $this->carrier->getOptionList($exclude);
        $options['status'] = config('common.status');
        $options['type'] = config('gateway.type');
        return view('admin.gateway')->with('options', $options);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new DataResult;
        
        try{
            $data = $request->all();
            $validator = Validator::make($request->all(), $this->repository->rules($data));
            if($validator->fails()) {
                $result->error = true;
                $result->message = $this->proccessErrorMessage($validator->errors());
            }
            else
            {
                $result->data = $this->repository->process($request);
                $result->message = 'Success';
            }

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function list(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->list($request);
            $result->data = $data;
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->getByID($id);
            $result->data = $this->repository->processForInput($data);
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $request->all();

            $result->data = $this->repository->delete($data[Constants::ID]);
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result);
    }

    public function keyValue()
    {
        $result = new DataResult;

        try{
        
            $result->data = $this->repository->keyValue();

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        return response()->json($result);
    }

    public function adjustBalance(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $request->all();
            $result->data = $this->repository->adjustBalance($data[Constants::ID], $data[Constants::ADJUST_AMOUNT]);
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        return response()->json($result);
    }
}
