<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Acme\Repositories\CreditPaymentRepository as Repository;
use Acme\Repositories\UserRepository as User;
use Acme\Common\CommonFunction;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\CreditPayment as DataField;
use Acme\Common\Entity\CreditPayment as Entity;
use Acme\Common\Constants as Constants;

class CreditPaymentController extends Controller
{

    use CommonFunction;
    protected $repository;
    public function __construct(Repository $repository, User $user)
    {
        // set the model
        $this->repository = $repository;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new DataResult;

        try{
            $input = $request->all();

            $entity = new Entity;
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->repository->save($data);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showByClient($id)
    {
        $client = $this->user->getByID($id);

        return view('admin.payment-history')->with('client', $client);
    }

    public function getList(Request $request)
    {
        $result = new DataResult;

        try{
            $result->data = $this->repository->list($request);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }
}
