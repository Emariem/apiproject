<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Acme\Common\CommonFunction;
use Acme\Repositories\AllocationTransactionRepository as AllocationTransaction;
use Acme\Common\DataResult as DataResult;

class AllocationTransactionController extends Controller
{
    use CommonFunction;
    protected $allocationTransaction;
    protected $result;

    public function __construct(AllocationTransaction $allocationTransaction, DataResult $result)
    {
        // set the model
        $this->allocationTransaction = $allocationTransaction;
        $this->result = $result;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listView(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $this->allocationTransaction->list($request);
            $result->data = $data;
            $result->error = false;
        }
        catch(Exception $e)
        {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }
}
