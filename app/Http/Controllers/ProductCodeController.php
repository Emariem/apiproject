<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Acme\Repositories\ProductCodeRepository as Repository;
use Acme\Repositories\CarrierRepository as Carrier;
use Acme\Common\CommonFunction;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\Gateway as DataField;
use Acme\Common\Entity\ProductCode as Entity;
use Acme\Common\Constants as Constants;



class ProductCodeController extends Controller
{
    //
    use CommonFunction;
    protected $repository;
    protected $carrier;
    public function __construct(Repository $repository, Carrier $carrier)
    {
        // set the model
        $this->repository = $repository;
        $this->carrier = $carrier;
    }
    

    public function index()
    {
        $exclude = [Constants::SYSTEM];
        $options['carriers'] = $this->carrier->getOptionList($exclude);
        $options['status'] = config('common.status');
        
        return view('admin.productcode.index')->with('options', $options);
    }

    public function list(Request $request)
    {
        $result = new DataResult;
        //
        try{
            $data = $this->repository->list($request);
            $result->data = $data;
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
       
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $result = new DataResult;
        //
        try{
            $input = $request->all();

            $entity = new Entity;
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->repository->create($data);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new DataResult;

        try{
            $input = $request->all();

            $entity = new Entity;
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->repository->save($data);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = new DataResult;
        try{
            $entity = new Entity;

            $data = $this->repository->getByID($id);
            $data = $entity->Deserialize($data);

            $result->message = 'Success';
            $result->data = $data;
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = new DataResult;
        //
        try{
            $input = $request->all();

            $entity = new Entity;
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->repository->update($data ,$id);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $result = new DataResult;

        try{
            $result->data = $this->repository->destroy($id);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        return response()->json($result, 200);
    }

    public function delete(Request $request)
    {
        $result = new DataResult;

        try{
            $input = $request->all();
            $result->data = $this->repository->delete($input[Constants::ID]);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

}
