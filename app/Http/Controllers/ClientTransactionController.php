<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Acme\Common\CommonFunction;
use Acme\Repositories\ClientTransactionRepository as Repository;
use Acme\Repositories\ClientAllocationRepository as ClientAllocationRepo;

use Acme\Common\DataResult as DataResult;
use Acme\Common\ApiKeys as ApiKeys;
use Acme\Common\Constants as Constants;

use Carbon\Carbon;

class ClientTransactionController extends Controller
{
    use CommonFunction;
    protected $repository;

    public function __construct(Repository $repository)
    {
        // set the model
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
    */

    /**
     * Process API transactions.
     *
     * @param  \Illuminate\Http\Request  $request
     * @bodyParam recipient_number string required The recipient_number of post. Example: 09388257463
     * @bodyParam register_to string required The Register To of post. Example: 343
     * @bodyParam product_code string required The product code of post. Example: 15
     * Transaction Successful.
     * @response {
     *      "message": "Success.",
     *      "data": {
     *          "TransactionID": "0000000000010",
     *          "AmountCharged": 10,
     *          "ProductName": "Regular Load 10"
     *      },
     *      "error": false,
     *      "tags": 0,
     *      "errorCodes": []
     *  }
     * @response{
     *      "message": "Request Failed.",
     *      "data": null,
     *      "error": true,
     *      "tags": 0,
     *      "errorCodes": [
     *          "601"
     *      ]
     *  }
     * @response{
     *      "message": "Request Failed.",
     *      "data": null,
     *      "error": true,
     *      "tags": 0,
     *      "errorCodes": [
     *          "602"
     *      ]
     *  }
     * @return \Illuminate\Http\Response
     */

    public function process(Request $request)
    {
        $result = new DataResult;
        //

        try{
            $input  = $request->all();
            $apikeys = new ApiKeys;
            $apikeys->SetData($input);
            $apikeys->Validate();
            $apikeys->Process();

            if(!$request->has("test"))
            {
                $apikeys->CheckCarrierReponse();
            }

            $result->error = $apikeys->hasError;
            $result->errorCodes = $apikeys->ErrorMessages;

            if($result->error)
            {
                $result->message = "Request Failed.";
            }
            else
            {
                $result->data = $apikeys->Iterate();
                $result->message = "Success.";
            }
            
            //$result->data = $apikeys->Iterate();
    
            //$test = $apikeys->CheckAvailableGateway();
            //$test = $apikeys->CheckProductCode();
            //$test = $apikeys->CheckClientBalance();

            //return $test;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $result = new DataResult;

        try{
            $data = $this->repository->getByID($id);

            $result->data = $data;
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listByClientAllocation(Request $request)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->listByClientAllocation($request);

            $total = $this->repository->getSumTotal($request);

            $result->tags = $total;
            $result->data = $data;
            $result->error = false;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    public function getTransactionByStatus(Request $request)
    {
        $result = new DataResult;
        
        try{
            $input = $request->all();
            $start = $input["start"];
            $end = $input["end"];
            $client_id = $input["client_id"];

            $current_user = $this->getCurrentUser();
            $success = $this->repository->getTransactionByStatus(
                        Constants::SUCCESS, 
                        $current_user , 
                        $start , 
                        $end , 
                        $client_id );

            $failed = $this->repository->getTransactionByStatus(
                        Constants::FAILED , 
                        $current_user , 
                        $start , 
                        $end , 
                        $client_id);

            $data["success"] = $success->count();
            $data["failed"] = $failed->count();

            $result->data = $data;
            $result->error = false;
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

     /**
     * Check Transaction Status.
     *
     * @param  $id
     * Transaction Status.
     * @response {
     *      "message": "",
     *      "data": {
     *           "TransactionID": "0000000000018",
     *           "RecipientNumber": "09954572368",
     *           "ProductCode": "GOSURF10",
     *           "Amount": 10,
     *           "Status": "Success",
     *           "Reference": "41904499",
     *           "created_at": "2019-05-27 21:20:31"
     *      },
     *      "error": false,
     *      "tags": 0,
     *      "errorCodes": []
     *}
     * @return \Illuminate\Http\Response
     */
    public function getTransactionStatus($id)
    {
        //
        $result = new DataResult;

        try{
            $status = $this->objectToArray(config('transactions.status'));

            $id = intval($id);

            $data = $this->repository->getByID($id);

            if($data)
            {
                $data->Status = $status[$data->Status];
                $data->Amount = $this->toAbsolute($data->Amount);
                $data->TransactionID = $this->StringPad(
                                                $data->TransactionID,
                                                13,
                                                "0",
                                                STR_PAD_LEFT);

                unset($data->AmountCharged);
                unset($data->Balance);
                unset($data->Type);
                unset($data->ClientID);
                unset($data->updated_at);
                unset($data->CarrierID);
                
                $result->data = $data;
                $result->error = false;
            }
            else
            {
                $result->message = "TransactionID Not found";
                $result->error = true;
            }
        
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * User's Transaction List.
     *
     * @param  $recipient
     * Transaction List.
     * @response {
     *      "message": "",
     *      "data": [{"TransactionID":"0000000000016","RecipientNumber":"09954572368","ProductCode":"GOSURF10","Amount":10,"Status":"Success","Reference":"43349006","created_at":"2019-05-27 21:13:57"},{"TransactionID":"0000000000017","RecipientNumber":"09954572368","ProductCode":"TEST","Amount":10,"Status":"Failed","Reference":"0","created_at":"2019-05-27 21:19:56"}],
     *      "error": false,
     *      "tags": 0,
     *      "errorCodes": []
     *}
     * @return \Illuminate\Http\Response
     */

    public function getByRecipient($recipient)
    {
        $result = new DataResult;

        try{
            $data = $this->repository->getByRecipient($recipient);

            $result->data = $data;

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    public function test()
    {
        $result = new DataResult;
        try{
            
            $allocation = new ClientAllocationRepo;
            $today = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
            $client_id = 4;


            $result->data = $data = $this->repository->getLatestReadTransaction(1);
            return response()->json($result, 200);

        }
        catch(Exception $e)
        {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
       
    }

    
}
