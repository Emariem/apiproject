<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
     //
     protected $table = 'userinfo';

     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
        'UserID', 'CompanyName', 'Credits', 'Interval', 'Type',
     ];

    public function account()
    {
        return $this->hasOne(User::class, 'id', 'UserID');
    }
}
