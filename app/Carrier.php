<?php

namespace App;
use Acme\Common\DataFields\Carrier as DataField;
use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
    //
    protected $table = DataField::TABLE_NAME;
}
