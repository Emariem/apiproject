<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ClientAllocation;
use Carbon\Carbon;

class AllocationTransaction extends Model
{
    protected $guarded = ['AllocationTransactionID'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d g:i a');
    }
    
    public function allocation()
    {
        return $this->hasOne(ClientAllocation::class, 'AllocationID', 'AllocationID');
    }
}
