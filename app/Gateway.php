<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Acme\Common\DataFields\Gateway as DataField;

class Gateway extends Model
{
    //
    protected $table = DataField::TABLE_NAME;
    protected $guarded = [DataField::GATEWAY_ID];
}
