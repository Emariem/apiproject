<?php

namespace App\Helpers;

use Acme\Repositories\ClientAllocationRepository as ClientAllocation;
use Acme\Repositories\AllocationTransactionRepository as AllocationTransaction;
use Acme\Repositories\ClientTransactionRepository as ClientTransaction;
use Acme\Repositories\CarrierRepository as Carrier;
use Acme\Common\DataFields\User as UserDataField;
use Acme\Common\DataFields\ClientAllocation as AllocationDataField;
use Acme\Common\DataFields\ClientTransaction as ClientTransactionDataField;
use Acme\Common\DataFields\AllocationTransaction as AllocTransactionDataField;
use Carbon\Carbon;
use Acme\Common\CommonFunction;
use Acme\Common\Constants;

class ClientAllocationHelper
{
    use CommonFunction;

    public static function addNewRecord($user , $remaining)
    {
        $clientAllocation = new ClientAllocation();
        $today = Carbon::now();
        $startDate = $today->startOfDay()->format('Y-m-d H:i:s');
        $daysToAdd = Constants::INTERVAL[$user->info->Interval];
        if($daysToAdd == 30) {
            $endDate = Carbon::now()->addMonths(1);
        } else {
            $endDate = Carbon::now()->addDays($daysToAdd);
        }
        $endDate = $endDate->startOfDay()->format('Y-m-d H:i:s');

        $budget = $user->info->Credits + $remaining;
        
        $data = [];
        $data['allocation_id'] = '';
        $data['client_id'] = $user->id;
        $data['interval'] = $user->info->Interval;
        $data['budget'] = $budget;
        $data['consumed'] = 0;
        $data['rate'] = 1;
        $data['status'] = 1;
        $data['start_date'] = $startDate;
        $data['end_date'] = $endDate;

        return $clientAllocation->create($data);
    }

    public static function addAllocationTransaction($user, $allocationID)
    {
        $allocationTransaction = new AllocationTransaction;

        $credits = $user->info->Credits;

        $data = [
            'allocation_transaction_id' => '',
            'allocation_id' => $allocationID,
            'credits' => $credits,
            'rate' => 1,
            'status' => Constants::ADJUSTMENT
        ];
        $allocationTransaction->create($data);
    }

    public static function addClientTransaction($user , $remaining)
    {
        $clientTransaction = new ClientTransaction;
        $carrierRepo = new Carrier();
        $carrier = $carrierRepo->getAdmin();
        $balance = $user->info->Credits + $remaining;

        $data = [
            'client_id' => $user->id,
            'recipient_number' => '',
            'product_code' => 'INITIAL CREDITS',
            'carrier_id' => $carrier['CarrierID'],
            'amount' => $user->info->Credits,
            'balance' => $balance,
            'type' => 5
        ];
        $clientTransaction->create($data);
    }

    public static function disablePreviousAllocation($allocationID)
    {
        $clientAllocation = new ClientAllocation();
        $clientAllocation->update([AllocationDataField::STATUS =>  0], $allocationID);
    }

}