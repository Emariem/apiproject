<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\UserInfo;
use App\MessageOut;
use App\MessageLog;
use App\MessageIn;
use App\CreditPayment;
use App\ClientTransaction;
use App\ClientCredit;
use App\ClientAllocation;
use App\AllocationTransaction;
use DB;

class RefreshDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trucate tables except admin user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(env('APP_MODE') == 'testing') {
            User::where('type', 2)->delete();
            UserInfo::truncate();
            MessageOut::truncate();
            MessageLog::truncate();
            MessageIn::truncate();
            CreditPayment::truncate();
            ClientTransaction::truncate();
            ClientCredit::truncate();
            ClientAllocation::truncate();
            AllocationTransaction::truncate();
            DB::table('oauth_access_tokens')->truncate();
            DB::table('oauth_auth_codes')->truncate();
            DB::table('oauth_clients')->truncate();
            DB::table('oauth_personal_access_clients')->truncate();
            DB::table('oauth_refresh_tokens')->truncate();
            DB::table('password_resets')->truncate();

            echo "Deleted!";
        }
    }
}
