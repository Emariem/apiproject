<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\ClientAllocation;
use App\ClientTransaction;

use Acme\Common\DataFields\User as UserDataField;
use Acme\Common\DataFields\ClientAllocation as AllocationDataField;
use Acme\Common\DataFields\ClientTransaction as ClientTransactionDataField;
use Acme\Common\DataFields\AllocationTransaction as AllocTransactionDataField;

use Acme\Repositories\ClientAllocationRepository as ClientAllocationRepo;
use Acme\Repositories\AllocationTransactionRepository as AllocationTransactionRepo;
use Acme\Repositories\ClientTransactionRepository as ClientTransactionRepo;
use Acme\Common\Constants;
use Acme\Common\CommonFunction;
use App\Helpers\ClientAllocationHelper;
use Carbon\Carbon;
use TheSeer\Tokenizer\Exception;
use Log;

class GenerateNextAllocationRecord extends Command
{
    use CommonFunction;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:nextallocation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if current client allocation 
                                has ended and generates new allocation record.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $today = Carbon::now();
            $users = User::select('id')->with('info')
                        ->with(['activeAllocation' => function($query)
                        {
                            $query->where(AllocationDataField::STATUS, Constants::ACTIVE);
                        }])
                        ->where(UserDataField::IS_ACTIVE, Constants::ACTIVE) 
                        ->where(UserDataField::IS_DELETED, 0)
                        ->where(UserDataField::TYPE, 2)
                        ->get();

            foreach($users as $user) {
                $activeAllocation = $user->activeAllocation;
                $info = $user->info;

                if($info->IsRecurring == 1) {
                    $remaining = $activeAllocation[AllocationDataField::BUDGET] - $activeAllocation[AllocationDataField::CONSUMED];

                    if($activeAllocation != null && $info->Interval != 0) {
                        $endDate = Carbon::createFromFormat(Constants::ROW_DATE_TIME_FORMAT, $activeAllocation->EndDate);
                        if($today > $endDate && $info->Interval > 0 ) {
                            $row = ClientAllocationHelper::addNewRecord($user , $remaining);
                            ClientAllocationHelper::disablePreviousAllocation($activeAllocation->AllocationID);
                            ClientAllocationHelper::addAllocationTransaction($user, $row->id , $remaining);
                            ClientAllocationHelper::addClientTransaction($user, $remaining);
                        }
                    } 
                }
                // else if($info->Interval != 0) {
                //     $row = ClientAllocationHelper::addNewRecord($user);
                //     ClientAllocationHelper::addAllocationTransaction($user, $row->id);
                //     ClientAllocationHelper::addClientTransaction($user);
                // }
            }
        } catch(\Exception $e) {
            $this->LogError(Constants::GEN_ALLOCATION_CMD_LOG, $e);
        }
        
    }

}
