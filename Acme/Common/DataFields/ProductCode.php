<?php
namespace Acme\Common\DataFields;

class ProductCode
{
    const TABLE_NAME = "product_code";

    const ID = "ProductCodeID";
    const CARRIER_ID = "CarrierID";
    const REGISTER_TO = "RegisterTo";
    const CODE = "Code";
    const NAME = "Name";
    const USSD_TEMPLATE = "USSDTemplate";
    const AMOUNT = "Amount";
    const AMOUNT_CHARGED = "AmountCharged";
    const STATUS = "Status";
    const IS_DELETED = "Is_Deleted";
}


?>