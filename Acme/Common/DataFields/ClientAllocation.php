<?php
namespace Acme\Common\DataFields;

class ClientAllocation
{
    const TABLE_NAME = "client_allocations";


    const ID = "AllocationID";
    const CLIENT_ID = "ClientID";
    const INTERVAL = "Interval";
    const BUDGET = "Budget";
    const CONSUMED = "Consumed";
    const RATE = "Rate";
    const START_DATE = "StartDate";
    const END_DATE = "EndDate";
    const SUCCESS = "Success";
    const FAILED = "Failed";
    const STATUS = "Status";
    const CREATED_AT = 'created_at';
}


?>