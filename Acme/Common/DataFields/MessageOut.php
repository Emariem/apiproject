<?php
namespace Acme\Common\DataFields;

class MessageOut
{

    const TABLE_NAME = "messageout";

    const ID = 'Id';        
    const MESSAGE_TO = 'MessageTo';
    const MESSAGE_FROM = 'MessageFrom';
    const MESSAGE_TEXT = 'MessageText';
    const MESSAGE_TYPE = 'MessageType';
    const MESSAGE_GUID = 'MessageGuid';
    const MESSAGE_INFO = 'MessageInfo';
    const GATEWAY = 'Gateway';
    const USER_ID = 'UserId';
    const USER_INFO = 'UserInfo';
    const PRIORITY = 'Priority';
    const SCHEDULED = 'Scheduled';
    const VALIDITY_PERIOD = 'ValidityPeriod';
    const IS_SENT = 'IsSent';
    const IS_READ = 'IsRead';
}


?>