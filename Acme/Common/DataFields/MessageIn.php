<?php
namespace Acme\Common\DataFields;

class MessageIn
{

    const TABLE_NAME = "messagein";

    const ID = 'Id';        
    const SEND_TIME = 'SendTime';
    const RECEIVE_TIME = 'ReceiveTime';
    const MESSAGE_FROM = 'MessageFrom';
    const MESSAGE_TO = 'MessageTo';
    const SMSC = 'SMSC';
    const MESSAGE_TEXT = 'MessageText';
    const MESSAGE_TYPE = 'MessageType';
    const MESSAGE_PARTS = 'MessageParts';
    const MESSAGE_PDU = 'MessagePDU';
    const GATEWAY = 'Gateway';
    const USER_ID = 'UserId';
    const SCANNED = 'Scanned';

}


?>