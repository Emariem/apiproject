<?php
namespace Acme\Common\DataFields;

class AllocationTransaction
{
    const TABLE_NAME = "allocation_transactions";


    const ID = "AllocationTransactionID";
    const ALLOCATION_ID = "AllocationID";
    const CREDITS = "Credits";
    const RATE = "Rate";
    const STATUS = "Status";
    const CREATED_AT = 'created_at';
}


?>