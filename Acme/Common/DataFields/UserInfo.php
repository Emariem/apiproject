<?php
namespace Acme\Common\DataFields;

class UserInfo
{
    const ID = "UserInfoID";
    const USER_ID = "UserID";
    const COMPANY_NAME = 'CompanyName';
    const CREDITS = 'Credits';
    const INTERVAL = 'Interval';
    const IS_RECURRING = 'IsRecurring';
}


?>