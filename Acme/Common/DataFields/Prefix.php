<?php
namespace Acme\Common\DataFields;

class Prefix
{
    const TABLE_NAME = "prefix";

    const ID = "PrefixID";
    const CODE = "Code";
    const CARRIER_ID = "CarrierID";
    
}


?>