<?php
namespace Acme\Common\DataFields;

class TransactionLog
{
    const TABLE_NAME = "transaction_logs";

    const ID = "TransactionLogID";
    const TRANSACTION_ID = "TransactionID";
    const CHARGED = "Charged";
    const GSM_BALANCE  = "GSMBalance";
    const GATEWAY_ID = "GatewayID";
    const DISCREPANCY  = "Discrepancy";
    const STATUS = "Status";
    const IS_READ = "IsRead";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";
}

?>