<?php
namespace Acme\Common;

class Constants
{
    #GENERAL
    const LIMIT = 20;
    const ALL = 'All';
    const SYMBOL_ALL = '*';
    const JSON = "json";
    const ID = "id";
    const ADJUST_AMOUNT = "adjust-amount";
    const SECRET = 'secret';
    const EMPTY = '';
    const ACTIVE = 1;
    const FIRST_INDEX = 0;
    const LINE_BREAK = '<br>';
    const PASSWORD_CONFIRMATION = 'password_confirmation';
    const INTERVAL = [ 0 => '', 1 => 0, 2 => 6, 3 => 30];


    const _TRUE = 1;
    const _FALSE = 0;


    const PlusCredits = 'plus_credits';
    const DeductCredits = 'deduct_credits';

    #GENERAL ERROR
    const ERROR_AUTHENTICATION = "Authentication Expired.";
    const LOST_CONNECTION = "Connection Failed.";

    #Pagination
    const PAGE_INDEX = "page";
    const PAGE_SIZE = "PageSize";
    const KEYWORD = "Search";
    const SORT_ORDER = "SortOrder";
    const SORT_BY = "SortBY";


    #FORMAT
    const INPUT_DATE_FORMAT     = 'Y-m-d';
    const OUTPUT_DATE_FORMAT    = 'F d,Y';
    const ROW_DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    const LIST_DATE_FORMAT = "m/d/Y";
    const LIST_DATE_TIME_FORMAT = "m/d/Y H:i:s";
    const ANDROID_DATETIME_FORMAT = 'yyyy-MM-dd hh:mm:ss';
    const ANDROID_DATE_FORMAT = 'yyyy-MM-dd';

    #DEFAULT VALUES
    const DEFAULT_SORT_ORDER = "DESC" ;
    const DEFAULT_SORT_BY = "id";

    const DESC = "DESC";
    const ASC = "ASC";

    #LOGIN
    const LOGIN_SUCCESS = "Successfully Logged-In";
    const LOGIN_FAILED = "Invalid Username and Password";
    const SUCCESSFULLY_REGISTERED = 'Successfully registered. You are now logged in';

    #VIEWS PAGES
    const ERROR_PAGE = "errors.errorpage";

    #ERROR_CODE
    const ERROR_CODE = "error_code";
    const ERROR_AUTHENTICATION_EXPIRED = 401;
    const ERROR_OUTDATED = 402;
    const ERROR_INACTIVE = 403;

    const ERROR_OUTDATED_APPLICATION = "The app you are using is outdated, Please contact organization administrator";
    const ERROR_INACTIVE_APPLICATION = "The app you are using was deactivated, Please contact organization administrator";
    #TIME
    const ONE_DAY = 86399;



    #API SETTINGS
    const RECHECK_COUNT = 5; 
    const SLEEP =  3;
    const TIME_GAP = 10;

    #TABLES
    const USERS = "users";
    const CARRIERS = "carriers";
    const GATEWAYS = "gateways";
    const CLIENT_ALLOCATIONS = 'client_allocations';
    const ALLOCATION_TANSACTIONS = 'allocation_transactions';

    #TRANSACTION_CODE
    const ADD_CREDITS = "ADD CREDITS";
    const REMOVE_CREDITS = "REMOVE CREDITS";


    #MODE
    const ADD = "add";
    const REMOVE = "remove";

    #USER TYPE
    const ADMIN = 1;
    const CLIENT = 2;

    #STATUS
    const SUCCESS = 1;
    const FAILED = 2;
    const PENDING = 0;
    const ADJUSTMENT = 4;
    const SYSTEM_STATUS = 5;

    #TRANSACTION_TYPE
    const CREDIT_ADD = 0;
    const CREDIT_REMOVE = 1;
    const CHARGE = 2;
    const LOAD_REQUEST = 3;
    #const ADJUSTMENT = 4;
    const INITIAL_CREDITS = 5;
    const REFUNDS = 6;

    #Carriers
    const SMART = 1;
    const GLOBE = 2;
    const SUN = 3;
    const SYSTEM  = 99;

    #PRODUCT_TYPE
    const REGULAR_LOAD = 1;
    const PROMOS = 2;


    const CONFIRM = 1;


    #MESSAGE_TYPE
    const GSM_USSD = "gsm.ussd";
    const GSM_DEFAULT = "Default";

    #USSD
    const GLOBE_USSD = 100;

    #INTERVAL
    const NONE_INTERVAL = 0;
    const DAILY = 1;
    const WEEKLY = 2;
    const MONTHLY = 3;

    #LOG CHANNELS
    const GEN_ALLOCATION_CMD_LOG = 'generateallocationlog';

}

?>