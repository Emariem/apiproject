<?php
namespace Acme\Common;

class Errors
{
    //API
    const CLIENT_USERNAME_REQUIRED = "601";
    const CLIENT_PASSWORD_REQUIRED = "602";
    const CLIENT_SECRET_REQUIRED = "603";

    const RECIPIENT_NUMBER_REQUIRED = "701";
    const REGISTER_TO_REQUIRED = "702";
    const PRODUCT_CODE_REQUIRED = "703";

    const INVALID_PRODUCT_CODE = "801";
    const NO_AVAILABLE_GATEWAY = "802";
    const NO_AVAILABLE_BALANCE = "803";
    const INVALID_RECIPIENT_FORMAT = "804";
    const INVALID_NETWORK_PREFIX = "805";

    const NETWORK_FAILED = "901";

    const SUCCESSFUL_TRANSACTION = "200";


    //MODULE

    const NO_ALLOCATION_FOUND = "No allocation found";
}