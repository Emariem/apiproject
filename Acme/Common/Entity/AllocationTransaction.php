<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\AllocationTransaction as DataField;

class AllocationTransaction
{
    public $AllocationTransactionID = "";
    public $AllocationID = "";
    public $Credits = "";
    public $Status ="";

    public function Validate()
    {
       
    }

    public function SetData($input)
    {
        $this->AllocationTransactionID = $input["allocation_transaction_id"];
        $this->AllocationID = $input["allocation_id"];
        $this->Credits = $input["credits"];
        $this->Rate = $input["rate"];
        $this->Status = $input["status"];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->AllocationTransactionID;
        $data[Datafield::ALLOCATION_ID] = $this->AllocationID;
        $data[Datafield::CREDITS] = $this->Credits;
        $data[Datafield::RATE] = $this->Rate;
        $data[Datafield::STATUS] = $this->Status;

        return $data;
    }
}


?>