<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\UserInfo as DataField;

class UserInfo
{
    public $UserInfoID = "";
    public $UserID = "";
    public $CompanyName = "";
    public $Interval = "";
    public $Credits ="";
    public $IsRecurring ="";

    public function Validate()
    {
       
    }

    public function SetData($input)
    {
        $this->UserID = $input["user_id"];
        $this->CompanyName = $input["company_name"];
        $this->Interval = $input["interval"];
        $this->Credits = $input["credits"];
        $this->IsRecurring = isset($input["is_recurring"])?$input["is_recurring"]:0;
    }

    public function Serialize()
    {
        $this->Validate();
        $data = array();

        $data[Datafield::USER_ID] = $this->UserID;
        $data[Datafield::COMPANY_NAME] = $this->CompanyName;
        $data[Datafield::INTERVAL] = $this->Interval;
        $data[Datafield::CREDITS] = $this->Credits;
        $data[Datafield::IS_RECURRING] = $this->IsRecurring;

        return $data;
    }
}


?>