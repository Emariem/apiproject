<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\CreditPayment as DataField;

class CreditPayment
{
    public $PaymentID = "";
    public $AllocationID = "";
    public $Amount = "";
    public $Date = "";

    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->PaymentID = $input["id"];
        $this->AllocationID = $input["allocation-id"];
        $this->Amount = $input["amount"];
        $this->Date = $input["date"];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->PaymentID;
        $data[Datafield::ALLOCATION_ID] = $this->AllocationID;
        $data[Datafield::AMOUNT] = $this->Amount;
        $data[Datafield::DATE] = $this->Date;

        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['allocation-id'] = $data[Datafield::ALLOCATION_ID];
        $result['amount'] = $data[Datafield::AMOUNT];
        $result['date'] = $data[Datafield::DATE];

        return $result;
    }

}


?>