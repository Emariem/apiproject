<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\Carrier as DataField;
use Acme\Common\Constants as Constants;


class Carrier
{
    public $CarrierID = 0;
    public $CarrierName = "System";
    public $Status = "1";
    public $CreatedAt = "";
    public $UpdatedAt = "";

    public function __construct()
	{
		$this->CarrierID = Constants::SYSTEM;
	}

    public function SetData($input)
    {
        $this->CarrierID = $input["id"];
        $this->CarrierName = $input["carrier_name"];
        $this->Status = $input["status"];
        $this->CreatedAt = $input["created_at"];
        $this->UpdatedAt = $input["updated_at"];

    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->CarrierID;
        $data[Datafield::CARRIER_NAME] = $this->CarrierName;
        $data[Datafield::STATUS] = $this->Status;

        return $data;
    }
}


?>