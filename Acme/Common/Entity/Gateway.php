<?php
namespace Acme\Common\Entity;

use Illuminate\Support\Facades\Validator;
use Acme\Common\DataFields\Gateway as DataField;

class Gateway
{
    public $GatewayID = "";
    public $Name = "";
    public $CarrierID = "";
    public $Address = "";
    public $SIM = "";
    public $Type = "";
    public $Balance = "";
    public $Minimum = "";
    public $Priority = "";
    public $Status = "";
    public $CreatedAt = "";
    public $UpdateAt = "";

    public $ErrorMessages = [];

    public function Validate()
    {
        $isValidate = true;
        $errorMessages = [];

        if($this->Name == "")
        {
            array_push($errorMessages,"Name is required.");
        }

        $this->ErrorMessages = $errorMessages;
        return $isValidate;
    }

    public function SetData($input)
    {
        $this->GatewayID = $input["id"];
        $this->Name = $input["name"];
        $this->CarrierID = $input["carrier_id"];
        $this->Address = $input["address"];
        $this->SIM = $input["sim"];
        $this->Type = $input['type'];
        $this->Balance = $input["balance"];
        $this->Minimum = $input['minimum'];
        $this->Priority = $input['priority'];
        $this->Status = $input['status'];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::GATEWAY_ID] = $this->GatewayID;
        $data[Datafield::NAME] = $this->Name;
        $data[Datafield::CARRIER_ID] = $this->CarrierID;
        $data[Datafield::ADDRESS] = $this->Address;
        $data[Datafield::SIM] = $this->SIM;
        $data[Datafield::TYPE] = $this->Type;
        $data[Datafield::BALANCE] = $this->Balance;
        $data[Datafield::MINIMUM] = $this->Minimum;
        $data[Datafield::PRIORITY] = $this->Priority;
        $data[Datafield::STATUS] = $this->Status;

        return $data;
    }
}


?>