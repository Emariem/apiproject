<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\ClientAllocation as DataField;

class ClientAllocation
{
    public $AllocationID = "";
    public $ClientID = "";
    public $Interval = "";
    public $Budget = "";
    public $Consumed ="";
    public $Rate ="";
    public $StartDate ="";
    public $EndDate ="";
    public $Success = 0;
    public $Failed = 0;
    public $Status ="";

    public function Validate()
    {
       
    }

    public function SetData($input)
    {
        $this->AllocationID = $input["allocation_id"];
        $this->ClientID = $input["client_id"];
        $this->Interval = $input["interval"];
        $this->Budget = $input["budget"];
        $this->Consumed = $input["consumed"];
        $this->Rate = $input["rate"];
        $this->StartDate = $input["start_date"];
        $this->EndDate = $input["end_date"];
        $this->Status = $input["status"];
        $this->Failed = 0;
        $this->Success = 0;
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->AllocationID;
        $data[Datafield::CLIENT_ID] = $this->ClientID;
        $data[Datafield::INTERVAL] = $this->Interval;
        $data[Datafield::BUDGET] = $this->Budget;
        $data[Datafield::CONSUMED] = $this->Consumed;
        $data[Datafield::RATE] = $this->Rate;
        $data[Datafield::START_DATE] = $this->StartDate;
        $data[Datafield::END_DATE] = $this->EndDate;
        $data[Datafield::STATUS] = $this->Status;
        $data[Datafield::SUCCESS] = $this->Success;
        $data[Datafield::FAILED] = $this->Failed;

        return $data;
    }
}


?>