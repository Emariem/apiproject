<?php
namespace Acme\Common\Entity;

use Acme\Common\DataFields\User as DataField;

class User
{
    public $UserID = "";
    public $Email = "";
    public $Username = "";
    private $Password = "";
    public $IsActive ="";
    public $Type = 2;
    public $IsDeleted = 0;

    public function Validate()
    {
       
    }

    public function SetData($input)
    {
        $this->UserID = $input["id"];
        $this->Email = $input["email"];
        $this->Username = $input["username"];
        $this->Password = $input["password"];
        $this->IsActive = $input["is_active"];
        $this->Type = $input["type"];
        $this->IsDeleted = isset($input["is_deleted"])?$input["is_deleted"] : 0;
    }

    public function Serialize()
    {
        $this->Validate();
        $data = array();

        $data[Datafield::ID] = $this->UserID;
        $data[Datafield::EMAIL] = $this->Email;
        $data[Datafield::USERNAME] = $this->Username;
        $data[Datafield::PASSWORD] = $this->Password;
        $data[Datafield::IS_ACTIVE] = $this->IsActive;
        $data[Datafield::TYPE] = $this->Type;
        $data[Datafield::IS_DELETED] = $this->IsDeleted;

        return $data;
    }

    public function Set($data)
    {
        $this->UserID = $data["id"];
        $this->Email = $data["email"];
        $this->Username = $data["username"];
        $this->Password = $data["password"];
        $this->IsActive = $data["is_active"];
        $this->Type = $data["type"];
    }
}


?>