<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\ProductCode as DataField;

class ProductCode
{
    public $ProductCodeID = "";
    public $CarrierID = "";
    public $RegisterTo = "";
    public $Code = "";
    public $Name = "";
    public $USSDTemplate = "";
    public $Amount = "";
    public $AmountCharged = "";
    public $Status = "";

    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->ProductCodeID = $input["id"];
        $this->CarrierID = $input["carrier_id"];
        $this->RegisterTo = $input["register_to"];
        $this->Code = $input["code"];
        $this->Name = $input["name"];
        $this->USSDTemplate = $input["ussd_template"];
        $this->Amount = $input["amount"];
        $this->AmountCharged = $input["amount_charged"];
        $this->Status = $input["status"];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->ProductCodeID;
        $data[Datafield::CARRIER_ID] = $this->CarrierID;
        $data[Datafield::REGISTER_TO] = $this->RegisterTo;
        $data[Datafield::CODE] = $this->Code;
        $data[Datafield::NAME] = $this->Name;
        $data[Datafield::USSD_TEMPLATE] = $this->USSDTemplate;
        $data[Datafield::AMOUNT] = $this->Amount;
        $data[Datafield::AMOUNT_CHARGED] = $this->AmountCharged;
        $data[Datafield::STATUS] = $this->Status;

        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['carrier_id'] = $data[Datafield::CARRIER_ID];
        $result['register_to'] = $data[Datafield::REGISTER_TO];
        $result['code'] = $data[Datafield::CODE];
        $result['name'] = $data[Datafield::NAME];
        $result['ussd_template'] = $data[Datafield::USSD_TEMPLATE];
        $result['amount'] = $data[Datafield::AMOUNT];
        $result['amount_charged'] = $data[Datafield::AMOUNT_CHARGED];
        $result['status'] = $data[Datafield::STATUS];

        return $result;
    }

}


?>