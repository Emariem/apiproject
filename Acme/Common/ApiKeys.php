<?php
namespace Acme\Common;

use Auth;

use Acme\Common\Constants as Constants;
use Acme\Common\Errors as Error;
use Acme\Common\CommonFunction as CommonFunction;

use Acme\Repositories\ClientTransactionRepository as ClientTransaction;
use Acme\Repositories\MessageOutRepository as MessageOut;
use Acme\Repositories\MessageInRepository as MessageIn;
use Acme\Repositories\ProductCodeRepository as ProductCode;
use Acme\Repositories\GatewayRepository as Gateway;
use Acme\Repositories\ClientAllocationRepository as ClientAllocation;
use Acme\Repositories\PrefixRepository as Prefix;
use Acme\Repositories\TransactionLogRepository as TransactionLog;

use Acme\Common\DataFields\Gateway as GatewayDataField;
use Acme\Common\DataFields\ProductCode as ProductCodeDataField;
use Acme\Common\DataFields\ClientTransaction as ClientTransactionDataField;
use Acme\Common\DataFields\ClientAllocation as ClientAllocationDataField;
use Acme\Common\DataFields\Prefix as PrefixDataField;
use Acme\Common\DataFields\MessageIn as MessageInDataField;

use Acme\Common\Entity\ClientTransaction as ClientTransactionEntity;
use Acme\Common\Entity\MessageOut as MessageOutEntity;
use Acme\Common\Entity\TransactionLog as TransactionLogEntity;

use Carbon\Carbon;

class ApiKeys
{
    use CommonFunction;

    public $ClientSecret = Constants::EMPTY ;
    public $ClientUserName = Constants::EMPTY ;
    public $ClientPassword = Constants::EMPTY ;
    public $RecipientNumber = Constants::EMPTY ;
    public $ProductCode = Constants::EMPTY ;
    public $RegisterTo = Constants::EMPTY ;

    private $CurrentBalance = 0;
    private $CarrierID = Constants::EMPTY ;
    private $ClientID = 0;
    private $GatewayID = Constants::EMPTY ;
    private $GatewayName = Constants::EMPTY ;
    private $AllocationID = 0;
    private $Amount = 0;
    private $AmountCharged = 0;
    private $ProductName = Constants::EMPTY;
    private $StandardNumberFormat  = "";
    private $TransactionID = 0;
    private $SendTime = "";
    private $User = "";
    private $NewBalance = 0;

    private $USSDTemplate = "";

    public $ErrorMessages = [];
    public $hasError = Constants::_FALSE;


    public function __construct()
    {
        $this->GetUserInfo();
    }

    private function GetUserInfo()
    {
        $user = "";
        if(Auth::guard('api')->user()){
            $user = Auth::guard('api')->user();
        }

        $this->User = $user;

        if($user)
        {
            $this->ClientID = $user[Constants::ID];
        }
    
    }

    public function Validate()
    {
       $result = Constants::_TRUE;

       if(trim($this->ClientSecret) == Constants::EMPTY )
       {
            #FROM Authentication
            //$this->pushError(Error::CLIENT_SECRET_REQUIRED);
            //$result =  Constants::_FALSE;
       }

       if(trim($this->ClientUserName) == Constants::EMPTY )
       {
           #FROM Authentication
           // $this->pushError(Error::CLIENT_USERNAME_REQUIRED);
           //$result =  Constants::_FALSE;
       }

       if(trim($this->ClientPassword) == Constants::EMPTY )
       {
            #FROM Authentication
            //$this->pushError(Error::CLIENT_PASSWORD_REQUIRED);
            //$result =  Constants::_FALSE;
       }

       if(trim($this->RecipientNumber) == Constants::EMPTY )
       {
            $this->pushError(Error::RECIPIENT_NUMBER_REQUIRED);
            $result =  Constants::_FALSE;
       }
       else
       {
           $this->RecipientNumber = $this->convertToStandard($this->RecipientNumber);
           $length  = strlen($this->RecipientNumber);
           $this->StandardNumberFormat = $this->RecipientNumber;
           if($length != 11)
           {
               $this->pushError(Error::INVALID_RECIPIENT_FORMAT);
               $result =  Constants::_FALSE;
           }
           else
           {
               if(!$this->CheckNetworkPrefix())
               {
                    $this->pushError(Error::INVALID_NETWORK_PREFIX);
                    $result =  Constants::_FALSE;
               }
           }
       }

       if(trim($this->ProductCode) == Constants::EMPTY )
       {
            $this->pushError(Error::PRODUCT_CODE_REQUIRED);
            $result =  Constants::_FALSE;
       }

       if(!$this->CheckProductCode())
       {
            $this->pushError(Error::INVALID_PRODUCT_CODE);
            $result =  Constants::_FALSE;
       }

       if(!$this->CheckClientBalance())
       {
            $this->pushError(Error::NO_AVAILABLE_BALANCE);
            $result =  Constants::_FALSE;
       }

       if(!$this->CheckAvailableGateway())
       {
            $this->pushError(Error::NO_AVAILABLE_GATEWAY);
            $result =  Constants::_FALSE;
       }

       $this->hasError = !$result;

       if($result)
       {
           //$this->pushError(Error::SUCCESSFUL_TRANSACTION);
       }

       return $result;
    }

    private function pushError($message)
    {
        array_push($this->ErrorMessages , $message);
    }

    public function CheckNetworkPrefix()
    {
        $repo = new Prefix;
        $prefix = $this->getMobilePrefix($this->RecipientNumber);
        $result = $repo->getByCode($prefix);

        if($result)
        {
             $this->CarrierID = $result[PrefixDataField::CARRIER_ID];
             return Constants::_TRUE;
        }
        else
        {
             return Constants::_FALSE;
        }
    }

    public function CheckProductCode()
    {
        $product_code = $this->ProductCode;
        $carrier_id = $this->CarrierID;

        $repo = new ProductCode;
        $result = $repo->getByCode($product_code, $carrier_id);
        
        if($result)
        {
            $this->Amount = $result[ProductCodeDataField::AMOUNT];
            $this->AmountCharged = $result[ProductCodeDataField::AMOUNT_CHARGED];
            $this->ProductName = $result[ProductCodeDataField::NAME];
            $this->RegisterTo = $result[ProductCodeDataField::REGISTER_TO];
            $this->USSDTemplate = $result[ProductCodeDataField::USSD_TEMPLATE];

            return Constants::_TRUE;
        }
        else
        {
            return Constants::_FALSE;
        }

    }

    public function CheckAvailableGateway()
    {
         $repo = new Gateway;

         $carrier = $this->CarrierID;
         $result = $repo->getAvailableGateway( $carrier, $this->AmountCharged);

         if($result)
         {
             $this->GatewayName = $result[GatewayDataField::NAME];
             $this->GatewayID = $result[GatewayDataField::GATEWAY_ID];
             return Constants::_TRUE;
         }
         else
         {
             return Constants::_FALSE;
         }
    }

    public function CheckClientBalance()
    {
        $date = date(Constants::INPUT_DATE_FORMAT);

        $repo = new ClientAllocation;
        $result = $repo->getCurrentAllocation($this->ClientID , $date);

        if($result)
        {
            $currentBalance = ($result[ClientAllocationDataField::BUDGET] - $result[ClientAllocationDataField::CONSUMED]);
            $this->CurrentBalance = $currentBalance;
            $this->AllocationID = $result[ClientAllocationDataField::ID];
            
            return $currentBalance>0?(Constants::_TRUE):(Constants::_FALSE);
        }
        else
        {
            return Constants::_FALSE;
        }

    }

    public function SetData($input)
    {
        $this->ClientSecret = isset($input["client_secret_key"])?$input["client_secret_key"]:Constants::EMPTY;
        $this->ClientUserName = isset($input["client_username"])?$input["client_username"]:Constants::EMPTY;
        $this->ClientPassword = isset($input["client_password"])?$input["client_password"]:Constants::EMPTY;
        $this->RecipientNumber = isset($input["recipient_number"])?$input["recipient_number"]:Constants::EMPTY;
        $this->ProductCode = isset($input["product_code"])?$input["product_code"]:Constants::EMPTY;
        $this->RegisterTo = isset($input["register_to"])?$input["register_to"]:Constants::EMPTY;
    }

    public function Serialize()
    {
        
    }

    public function Process()
    {
        if(!$this->hasError)
        {
            $this->SaveTransaction();
            $this->SaveMessageOut();
            $this->SaveTransactionLog();
        }

        return !$this->hasError;
    }


    private function SaveTransaction()
    {
        $repo = new ClientTransaction;
        $entity = new ClientTransactionEntity;

        $this->AdjustCredits();

        $input = [];
        $input["client_id"] = $this->ClientID;
        $input["recipient_number"] = $this->RecipientNumber;
        $input["product_code"] = $this->ProductCode;
        $input["carrier_id"] = $this->CarrierID;
        $input["amount"] = $this->negative($this->Amount);
        $input["amount_charged"] = $this->negative($this->AmountCharged);
        $input["balance"] = $this->NewBalance;
        $input["transaction_type"] = 3;

        $result = $repo->create($input);
        $this->TransactionID = $result->id;

    }

    private function SaveTransactionLog()
    {
        $entity = new TransactionLogEntity;
        $repo = new TransactionLog;

        $input = [];
        $input["transaction_id"] = $this->TransactionID;
        $input["charged"] = 0;
        $input["gsm_balance"] = 0;
        $input["gateway_id"] = $this->GatewayID;
        $input["discrepancy"] = 0;
        $input["status"] = 0;
        $input["is_read"] = 0;

        $entity->SetData($input);
        $repo->save($entity->Serialize());

    }

    private function SaveMessageOut()
    {
        $repo = new MessageOut;
        $entity = new MessageOutEntity;

        $text = "";
        $register_to = "";
        
        if($this->CarrierID != Constants::GLOBE)
        {
            $text = $this->ProductCode." ".$this->RecipientNumber;
            $register_to = $this->RegisterTo;
            $message_type = Constants::GSM_DEFAULT;
        }
        else
        {
            $register_to = $this->createUSSD();
            $message_type = Constants::GSM_USSD;
        }

        $input = [];
        $input["message_to"] = $register_to;
        $input["message_from"] = $this->RecipientNumber;
        $input["message_text"] = $text;
        $input["gateway"] = $this->GatewayName;
        $input["user_id"] = $this->TransactionID;
        $input["message_type"] = $message_type;

        $entity->SetData($input);
        $data = $entity->Serialize();
            //
        $repo->create($data);
        $this->SendTime = Carbon::now()->format(Constants::ROW_DATE_TIME_FORMAT);
    }

    private function createUSSD()
    {
        $template = str_replace("{number}", $this->RecipientNumber , $this->USSDTemplate);
        return $template;

    }

    public function CheckCarrierReponse()
    {
        $repo = new MessageIn;

        $start = $this->SendTime;
        $end = Carbon::parse($this->SendTime)
                        ->addSeconds(Constants::TIME_GAP)
                        ->format(Constants::ROW_DATE_TIME_FORMAT);
        $code = $this->ProductName;
        $recipient = $this->RecipientNumber;

        $count = 0;
        $result = [];

        if($this->hasError) {
            return null;
        }
  
        while(count($result) == 0 && $count < Constants::RECHECK_COUNT)
        {
            $result = $repo->getBySendTime($start ,$end , $code , $recipient);
            if(count($result) == 0) {
                sleep(Constants::SLEEP);
            }
            $count++;
        }

        if(count($result) > 0) {

            $message = $result[0];
            $data = [];
            if($this->CarrierID == Constants::GLOBE)
            {
                $data = $this->TextMessageParseData($message[MessageInDataField::MESSAGE_TEXT]);
            }
            else
            {
                $data = $this->TextMessageParseDataGlobe($message[MessageInDataField::MESSAGE_TEXT]);
            }
            
            $transaction_id = $this->TransactionID;
            $reference = $data['ref'];

            $transaction_repo = new ClientTransaction;
            $transaction_repo->updateReferenceNumber($transaction_id ,$reference);

            $gateway_id = $this->GatewayID;
            $charged = $this->negative($data["cost"]);

            $gateway = new Gateway;
            $gateway->adjustBalance($gateway_id,$charged);
        }

        return $result;
    }

    private function AdjustCredits()
    {
        $repo = new ClientAllocation;
        $client_id = $this->ClientID;
        $current_date = Carbon::now()->format(Constants::ROW_DATE_TIME_FORMAT);

        $allocation_id = $this->AllocationID;
        $current_balance = $this->CurrentBalance;
        $amount = $this->toAbsolute($this->Amount);

        $this->NewBalance = $current_balance - $amount;

        $repo->updateNumberOfTransactions($allocation_id, 1 ,Constants::SUCCESS);
        $result = $repo->adjustConsumed($allocation_id , $amount);
    }

    public function Iterate()
    {
        
        $objects["TransactionID"] = $this->StringPad(
                                                $this->TransactionID,
                                                13,
                                                "0",
                                                STR_PAD_LEFT);
        //$objects["CurrentBalance"] = $this->NewBalance;
        //$objects["CarrierID"]= $this->CarrierID;
        //$objects["ClientID"]= $this->ClientID;
        //$objects["AllocationID"]= $this->AllocationID;
        //$objects["GatewayName"]= $this->GatewayName;
        //$objects["Amount"]= $this->Amount;
        $objects["AmountCharged"]= $this->Amount;
        $objects["ProductName"]= $this->ProductName;
        //$objects["StandardNumberFormat"] = $this->StandardNumberFormat;

        return $objects;
    }



}

?>