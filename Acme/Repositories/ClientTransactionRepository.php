<?php
namespace Acme\Repositories;

use App\ClientTransaction as Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Acme\Common\DataFields\ClientTransaction as DataField;
use Acme\Common\DataFields\User as UserDataField;
use Acme\Common\DataFields\MessageOut as MessageOutDataField;
use Acme\Common\DataFields\TransactionLog as LogDataField;

use Acme\Common\Entity\ClientTransaction as Entity;
use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;
use Acme\Common\CommonFunction;

use Acme\Repositories\MessageLogRepository as MessageLog;
use Acme\Repositories\ClientAllocationRepository as ClientAllocation;

class ClientTransactionRepository extends Repository{

    protected $model;

    use Pagination;
    use CommonFunction;
    
    public function __construct()
    {
        $this->model = new Model;
        $this->messageLog = new MessageLog;
        $this->SortBy = DataField::TRANSACTION_ID;
    }

    public function getByID($id){
        $result = $this->model->where(DataField::TRANSACTION_ID, $id)->first();

        return $result;
    }

    public function getByClientID($client_id){
        $result = $this->model->where(DataField::CLIENT_ID, $client_id)->get();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $query = $this->model->where(DataField::STATUS,'1');
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                $query->where(DataField::RECIPIENT_NUMBER, 'LIKE', '%' . $search . '%');
            });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $result =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);
        
        
        // $result = $result->map(function($item){
        //     $item->date = Carbon::parse($item->created_at)->format('M j');
        //     $item->time = Carbon::parse($item->created_at)->format('g:ia');
        //     $item->productCode = ($item->Credits < 0) ? 'CREDIT ADD' : 'CREDIT ADJUST';
        //     $item->carrier = 'Admin';

        //     return $item;
        // });

        return $result;
    }

    public function listByClientAllocation($request){
        $this->SetPage($request);
        $input = $request->all();
        $query = $this->model->with('carrier')
                    ->where(DataField::CLIENT_ID, $input[Constants::ID]);
        
        if($request->has('startDate') && $request->has('endDate')) {
            $query->whereDate(DataField::CREATED_AT, '>=', $request->input('startDate'))
                  ->whereDate(DataField::CREATED_AT, '<=', $request->input('endDate'));
        }

        if($request->has('type')) {
            $query->where(DataField::TYPE, $request->input('type'));
        }

        if($request->has('carrier')) {
            $query->where(DataField::CARRIER_ID, $request->input('carrier'));
        }

        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated  =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);
        $types = $this->objectToArray(config('transactions.type'));
        $status = $this->objectToArray(config('transactions.status'));
        $transformedData = $paginated->getCollection()->transform(function($item) use ($types , $status){
            $item->date = Carbon::parse($item->created_at)->format('M j');
            $item->time = Carbon::parse($item->created_at)->format('g:ia');
            $item->Balance = $this->formatDouble($item->Balance);
            $item->StatusName = $status[$item->Status];
            $item->TransactionID = $this->StringPad(
                                                $item->TransactionID,
                                                13,
                                                "0",
                                                STR_PAD_LEFT);
            if($item->StatusName == 'Failed' || $item->ProductCode == Constants::REMOVE_CREDITS) {
                $item->Positive = false;
                $item->Class = 'tr-red';
            } else {
                $item->Positive = true;
                $item->Class = 'tr-green';
            }
            return $item;
        });

        $transaformedPaginated = new LengthAwarePaginator(
            $transformedData,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), 
            [
                'path' => $request->url(),
                'query' => [ 'page' => $paginated->currentPage() ]
            ]);
            
        return $transaformedPaginated;
    }


    public function getSumTotal($request){
        $result = 0;
        $input = $request->all();
        $query = $this->model->selectRaw('SUM('.DataField::AMOUNT.') as Amount,
                SUM('.DataField::AMOUNT_CHARGED.') as AmountCharged')
                ->where(DataField::CLIENT_ID, $input[Constants::ID]);

        if($request->has('startDate') && $request->has('endDate')) {
            $query->whereDate(DataField::CREATED_AT, '>=', $request->input('startDate'))
                  ->whereDate(DataField::CREATED_AT, '<=', $request->input('endDate'));
        }

        if($request->has('type')) {
            $query->where(DataField::TYPE, $request->input('type'));
        }

        if($request->has('carrier')) {
            $query->where(DataField::CARRIER_ID, $request->input('carrier'));
        }
        
        $result = $query->first();

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function destroyByClientID($client_id){
        $result = $this->model->where(DataField::CLIENT_ID, $client_id)->delete();
        return $result;
    }

    public function create($input){
       $result = null;
       $entity = new Entity;

       $entity->SetData($input);
       return $this->model->create($entity->serialize());
    }

    public function update($request , $id){
       $input = $request->all();
       $result= $this->model->where(DataField::ID,$id)->update($input);

       return $result;
    }

    public function save($request){
        $input = $request->all();
        $result = null;
        
        if(!isset($input[Constants::ID])){
           $result = $this->model->create($input);
        }
        else{
           $result = $this->model->where(DataField::ID, $input[Constants::ID])->update($input);
        }

        return $result;
    }

    public function processInput($input)
    {
        $entity = new Entity;
        $entity->SetData($input);

        $result = $this->model->create($entity->Serialize());

        return $result;
    }


    public function process($request){
        $input = $request->all();
       
        $entity = new Entity;
        $entity->SetData($input);

        $result = $this->model->create($entity->Serialize());

        return $result;
    }

    public function apiTransaction($request){
        $input = $request->all();
       
        $entity = new Entity;
        $entity->SetData($input);

        $result = $this->model->create($entity->Serialize());

        return $result;
    }

    public function searchTranscation($recipient , $send_time , $product_code)
    {
        $gap = Carbon::parse($send_time)->addSeconds(-10)->format(Constants::ROW_DATE_TIME_FORMAT);

        $result = $this->model
                       ->where(DataField::RECIPIENT_NUMBER,$recipient)
                       ->where(DataField::STATUS,0)
                       ->whereBetween(DataField::CREATED_AT,[$gap , $send_time])
                       ->first();
        return $result;
    }

    public function updateReferenceNumber($id , $reference)
    {
        $result = $this->model
                       ->where(DataField::TRANSACTION_ID,$id)
                       ->update([
                           DataField::REFERENCE => $reference,
                           DataField::STATUS => Constants::SUCCESS
                        ]);

        return $result;
    }

    public function getTransactionByStatus( $status , $current_user , $start , $end , $client_id)
    {

        $query = $this->model
                      ->where(DataField::STATUS, $status)
                      ->whereDate(DataField::CREATED_AT, ">=", $start)
                      ->whereDate(DataField::CREATED_AT, "<=", $end);

        if($current_user->Type == Constants::ADMIN) {
            if($client_id != Constants::EMPTY) {
                $query->where(DataField::CLIENT_ID, $client_id);
            }
        } else {
            $query->where(DataField::CLIENT_ID, $current_user->UserID);
        }

        $result = $query;
                      
        return $result;
    }

    public function searchAndMarkTransactionAsFailed()
    {
        $adjusted = Carbon::now()->subMinutes(10)->format(Constants::ROW_DATE_TIME_FORMAT);

        $builder = $this->model
                       ->where(DataField::CREATED_AT, "<=", $adjusted)
                       ->where(DataField::STATUS, "=", Constants::PENDING)
                       ->where(DataField::TYPE, "=", Constants::LOAD_REQUEST);

        $allocation = new ClientAllocation;
        
        $list = $builder->get()->map(function($item) use($allocation) {
            $entity = new Entity;
            $entity->Format($item);

            $client_id = $entity->ClientID;
            $currentDate = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
            $currentAllocation = $allocation->getCurrentAllocation($client_id, $currentDate);

            $balance = ($currentAllocation->Budget - $currentAllocation->Consumed) + $this->toAbsolute($item->AmountCharged);
             
            $entity->TransactionID = "";
            $entity->Reference = $item->TransactionID;
            $entity->Status = Constants::REFUNDS;
            $entity->Type = Constants::REFUNDS;
            $entity->Amount = $this->toAbsolute($item->Amount);
            $entity->AmountCharged = $this->toAbsolute($item->AmountCharged);
            $entity->Balance = $balance;

            $this->model->create($entity->Serialize());

            // $allocation->adjustBudget($currentAllocation->AllocationID ,$entity->AmountCharged);
            $allocation->adjustConsumed($currentAllocation->AllocationID ,$item->AmountCharged);
            $allocation->updateNumberOfTransactions($currentAllocation->AllocationID, 1 ,Constants::FAILED);
            return $item;
        });

        $update =  new Model;
        $update->where(DataField::CREATED_AT, "<=", $adjusted)
                ->where(DataField::STATUS, "=", Constants::PENDING)
                ->update([DataField::STATUS => Constants::FAILED]);

        return $list;
    }

    public function getByRecipient($recipient)
    {
        $status = $this->objectToArray(config('transactions.status'));

        $builder = $this->model
                        ->where(DataField::RECIPIENT_NUMBER, "=", $recipient);
        
        $list = $builder->get()->map(function($data) use($status){
                $data->Status = $status[$data->Status];
                $data->Amount = $this->toAbsolute($data->Amount);
                $data->TransactionID = $this->StringPad(
                                                $data->TransactionID,
                                                13,
                                                "0",
                                                STR_PAD_LEFT);

                unset($data->AmountCharged);
                unset($data->Balance);
                unset($data->Type);
                unset($data->ClientID);
                unset($data->updated_at);
                unset($data->CarrierID);

                return $data;
        });

        return $list;
    }

    public function getByGateway($gateway_id)
    {
        $builder = $this->model
                        ->join(LogDataField::TABLE_NAME,
                                    DataField::TABLE_NAME.".".DataField::TRANSACTION_ID,
                                    '=', 
                                    LogDataField::TABLE_NAME.".".LogDataField::TRANSACTION_ID);

        $builder->where(LogDataField::GATEWAY_ID,"=",$gateway_id)
                ->where(DataField::TYPE,"=",Constants::LOAD_REQUEST);

        $builder->where(function ($q){ 
            $q->where(DataField::TABLE_NAME.".".DataField::STATUS,"=",Constants::SUCCESS)
              ->orWhere(DataField::TABLE_NAME.".".DataField::STATUS,"=",Constants::PENDING)
              ->orWhere(DataField::TABLE_NAME.".".DataField::STATUS,"=",Constants::FAILED);

        });
                    
        $result = $builder
                  ->select(
                        DataField::TABLE_NAME.".".DataField::TRANSACTION_ID,
                        DataField::CARRIER_ID,
                        DataField::TYPE,
                        DataField::TABLE_NAME.".".DataField::STATUS,
                        DataField::REFERENCE,
                        DataField::TABLE_NAME.".".DataField::CREATED_AT,
                        LogDataField::GATEWAY_ID,

                    )
                  ->orderBy(
                    DataField::TABLE_NAME.".".DataField::CREATED_AT,
                    Constants::DESC)
                  ->get();

        return $result;
    }

    public function updateAndRemoveScanned($id)
    {
        $this->model->where(DataField::ID,$id)->update([DataField::STATUS=>Constants::SUCCESS]);
        $this->model->where(DataField::REFERENCE, $id)->delete();
    }

    public function getLatestReadTransaction($gateway_id)
    {
        $builder = $this->model
                        ->join(LogDataField::TABLE_NAME,
                                    DataField::TABLE_NAME.".".DataField::TRANSACTION_ID,
                                    '=', 
                                    LogDataField::TABLE_NAME.".".LogDataField::TRANSACTION_ID);

        $result = $builder->where(LogDataField::GATEWAY_ID,"=",$gateway_id)
                            ->where(LogDataField::IS_READ,"=",1)
                            ->orderBy(
                                DataField::TABLE_NAME.".".DataField::CREATED_AT,
                                Constants::DESC)
                            ->first();

        return $result;
    }

}