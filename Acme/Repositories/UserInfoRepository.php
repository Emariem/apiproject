<?php
namespace Acme\Repositories;

use App\UserInfo;
use Illuminate\Support\Facades\Validator;
use Acme\Common\DataResult as DataResult;
use Acme\Common\Constants as Constants;
use Acme\Common\DataFields\UserInfo as DataField;
use Acme\Common\Entity\UserInfo as Entity;

class UserInfoRepository extends Repository{

    protected $model;
    
    public function __construct()
    {
        $this->model = new UserInfo;
    }
    
    public function rules()
    {
        $rules = [
            'company_name' => ['required', 'string', 'max:255'],
            'credits' => ['required'],
            'interval' => ['required'],
        ];
        return $rules;
    }


    public function findId($id){
        return $this->model->where('id', $id)->first();
    }

    public function show($id){
        return $this->model->find($id);
    }

    public function getByUserID($user_id){
        return $this->model->where(DataField::USER_ID, $user_id)->first();
    }

    public function destroy($id){
        $this->model->where('id', $id)->delete();
    }

    public function destroyByClientID($client_id){
        $this->model->where(DataField::USER_ID, $client_id)->delete();
    }

    public function save($request){
        $input = $request->all();
        
        if(!isset($input['id'])){
            $this->model->create($input);
        }
        else{
            $this->model->where('id', $input['id'])->update($input);
        }

        return ['status' => true, 'results' => 'Success'];
    }

    public function updateByUser($data){
        $result = new DataResult;
        $this->model->where('userId', $data['userId'])->update($data);

       $result->message = "Success!";
       return $result;
    }

    public function getChanges($input)
    {
        $this->model->fill($input);

        $result = $this->model->getDirty();
    }
    public function processClientInfo($input)
    {
        $entity = new Entity;
        $entity->SetData($input);
        
        $this->model->where(DataField::USER_ID, $input[Constants::ID])->update($entity->serialize());
    }

    public function getClients()
    {
        $result = $this->model::with('info')
                    ->where(DataField::IS_ACTIVE, 1)
                    ->get();
        return $result;
    }

    public function process($input)
    {
        $result = null;
        $entity = new Entity;

        $entity->SetData($input);
        $this->model->create($entity->serialize());
    }

    public function processForInput($row)
    {
        $data = array();
        $data["user_id"] = $row->UserID;
        $data["company_name"] = $row->CompanyName;
        $data["interval"] = $row->Interval;
        $data["credits"] = $row->Credits;
        $data["plus_credits"] = 0;
        $data["deduct_credits"] = 0;
        $data['is_recurring'] = $row->IsRecurring;

        return $data;
    }
}