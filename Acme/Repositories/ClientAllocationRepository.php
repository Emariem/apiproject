<?php
namespace Acme\Repositories;

use App\ClientAllocation as Model;
use Illuminate\Support\Facades\Validator;

use Acme\Common\DataFields\ClientAllocation as DataField;
use Acme\Common\DataFields\AllocationTransaction as AllocDataField;
use Acme\Common\DataFields\UserInfo as InfoDataField;
use Acme\Repositories\UserInfoRepository as UserInfoRepo;

use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;
use Acme\Common\Entity\ClientAllocation as Entity;

use Exception;
use Carbon\Carbon;
use Acme\Common\CommonFunction;
use Symfony\Component\VarDumper\Cloner\Data;

class ClientAllocationRepository extends Repository{

    protected $model;

    use Pagination;
    use CommonFunction;
	
	public function __construct()
	{
        $this->model = new Model;
        $this->SortBy = DataField::ID;
	}

     public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $query = $this->model->where(DataField::STATUS,'1');
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                $query->where(DataField::CARRIER_NAME, 'LIKE', '%' . $search . '%');
            });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $result =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);
        
        return $result;
    }

    public function listByClient($request){
        $this->SetPage($request);
        $query = $this->model->where(DataField::STATUS,'1');
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                $query->where(DataField::CARRIER_NAME, 'LIKE', '%' . $search . '%');
            });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $result =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);
        
        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function destroyByClientID($client_id){
        $result = $this->model->where(DataField::CLIENT_ID, $client_id)->delete();

        return $result;
    }

    public function create($input){
        $result = null;
        $entity = new Entity;

        $entity->SetData($input);
        return $this->model->create($entity->serialize());
    }

    public function update($input , $id){
    //    $input = $request->all();
       $result= $this->model->where(DataField::ID,$id)->update($input);

       return $result;
    }

    public function save($request){
        $input = $request->all();
        $result = null;
        
        if(!isset($input[Constants::ID])){
           $result = $this->model->create($input);
        }
        else{
           $result = $this->model->where(DataField::ID, $input[Constants::ID])->update($input);
        }

        return $result;
    }

    public function getByClientID($id)
    {
        $result = $this->model->where(DataField::CLIENT_ID, $id)
                    ->where(DataField::STATUS, 1)
                    ->first();

        return $result;
    }

    public function getAllByClientID($id)
    {
        $result = $this->model->with('payments')->where(DataField::CLIENT_ID, $id)->get();
        $result = $result->map(function($item) {
            $item->StartDate = Carbon::parse($item->StartDate)->format('M j, Y');
            $item->EndDate = Carbon::parse($item->EndDate)->format('M j, Y');
            $total = 0;

            foreach($item->payments as $payment) {
                $total += $payment->Amount;
            }

            $item->TotalPaid = $total;
            
            if($total == $item->Consumed) {
                $item->PaymentStatus = 'Paid';
            } else if($total < $item->Consumed && $total > 0) {
                $item->PaymentStatus = 'Partially paid';
            } else if($total == 0) {
                $item->PaymentStatus = 'Pending payment';
            } else {
                $item->PaymentStatus = '';
            }
            return $item;
        });

        return $result;
    }


    public function getAsOptions($id)
    {
        $sqlDateFormat = '%b %d, %Y';
        $startDate = "DATE_FORMAT(".DataField::START_DATE.", '".$sqlDateFormat."')";
        $endDate = "DATE_FORMAT(".DataField::END_DATE.", '".$sqlDateFormat."')";
        $select = DataField::ID.", CONCAT(".$startDate.", ' - ', ".$endDate.") AS DateRange";
        $result = $this->model->selectRaw($select)
                        ->where(DataField::CLIENT_ID, $id)
                        ->get()
                        ->pluck('DateRange', DataField::ID);

        return $result;
    }

    public function adjustBudget($id, $credits){
        // if($credits < 0) {
        $this->model->where(DataField::ID, $id)->increment(DataField::BUDGET, $credits);
        $result = $this->getByID($id);
        return $result;
        // } else if($credits > 0) {
        //     $this->model->where(DataField::ID, $id)->decrement(DataField::BUDGET, $credits);
        // }
    }

    public function adjustConsumed($id, $charged)
    {
        $this->model->where(DataField::ID, $id)->increment(DataField::CONSUMED, $charged);
        $result = $this->getByID($id);
        return $result;
    }

    public function adjustBudgetByClientID($client_id , $date , $credits)
    {
        $allocation = $this->getCurrentAllocation($client_id , $date);
        $id = $allocation[DataField::ID];

        $this->adjustBudget($id , $credits);

        return $allocation;
    }

    public function process($input)
    {
        $result = null;
        $entity = new Entity;

        $entity->SetData($input);
        return $this->model->create($entity->serialize());
    }

    public function getLoadedCredits($request)
    {
        $result = [];
        $budget = 0;
        $balance = 0;
        
        if($request->has('clientID')) {
            $client_id = $request->input('clientID');
            $today = Carbon::now()->format(Constants::INPUT_DATE_FORMAT);
            $allocation = $this->getCurrentAllocation($client_id, $today);
            $budget = $allocation[DataField::BUDGET];
            $balance = $budget - $allocation[DataField::CONSUMED];

        } else {
            $start = $request->input('startDate');
            $end = $request->input('endDate');

            $query = $this->model->selectRaw('SUM('.DataField::BUDGET.') as Budget,
                SUM('.DataField::BUDGET.' - '.DataField::CONSUMED.') as Balance');

            $query->where(function ($q) use ($start , $end){ 
                $q->whereDate(DataField::START_DATE,'<=', $start)
                  ->whereDate(DataField::END_DATE, '>=', $start);
            })->orWhere(function($q) use ($end){
                $q->whereDate(DataField::START_DATE, '<=',$end)
                  ->whereDate(DataField::END_DATE, '>=', $end);
            })->orWhere(function($q) use ($start , $end){
                $q->whereDate(DataField::START_DATE,'>=', $start)
                  ->whereDate(DataField::END_DATE, '<=', $end);
            })->orWhere(function($q) use($start, $end) {
                $q->WhereNull(DataField::START_DATE)
                  ->WhereNull(DataField::END_DATE)
                  ->whereDate(DataField::CREATED_AT, '>=', $start)
                  ->whereDate(DataField::CREATED_AT, '<=', $end);
            });



            $allocation = $query->first();
            $budget = $allocation->Budget;
            $balance = $allocation->Balance;
        }

        $result['totalLoaded'] = $this->formatDouble($budget);
        $result['totalBalance'] = $this->formatDouble($balance);
        return $result;
    }

    public function getCurrentAllocation($client_id, $date)
    {
        $result = null;

        try{
            $user_info = new UserInfoRepo;
            $info = $user_info->getByUserID($client_id);

            if($info->Interval == Constants::NONE_INTERVAL)
            {
                $result = $this->model
                        ->where(DataField::CLIENT_ID, $client_id)
                        ->where(DataField::INTERVAL, Constants::NONE_INTERVAL)
                        ->first();
                        
            }
            else
            {
                $result = $this->model->where(DataField::CLIENT_ID, $client_id)
                        ->whereDate(DataField::START_DATE, '<=', $date)
                        ->whereDate(DataField::END_DATE, '>=', $date)
                        ->first();
            }
        }
        catch(Exception $e)
        {
            return null;
        }
        
        return $result;
    }

     public function updateNumberOfTransactions($id, $quantity ,$status)
    {
        $allocation = $this->getByID($id);
        
        $field = $status==Constants::SUCCESS?DataField::SUCCESS:DataField::FAILED;

        $additional = $allocation[$field] + $quantity;

        $result = null;
        $result= $this->model->where(DataField::ID,$id)->update([$field =>$additional]);

        return $result;
    }

}