<?php
namespace Acme\Repositories;

use App\AllocationTransaction as Model;
use Illuminate\Support\Facades\Validator;

use Acme\Common\DataFields\AllocationTransaction as DataField;
use Acme\Common\DataFields\ClientAllocation as ClientAllocationDataField;
use Acme\Common\DataFields\ClientTransaction as ClientTransactionDataField;
use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;
use Illuminate\Pagination\LengthAwarePaginator;
use Acme\Common\Entity\AllocationTransaction as Entity;
use Acme\Repositories\UserInfoRepository as UserInfo;
use Acme\Repositories\ClientAllocationRepository as ClientAllocation;
use Acme\Repositories\ClientTransactionRepository as ClientTransaction;
use Carbon\Carbon;

class AllocationTransactionRepository extends Repository{

    protected $model;

    use Pagination;
	
	public function __construct()
	{
        $this->model = new Model;
        $this->clientAllocation = new ClientAllocation;
        $this->clientTransaction = new ClientTransaction;
        $this->SortBy = DataField::ID;
	}

     public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $id = $request->input(Constants::ID);
        $query = $this->model->join('client_allocations', function($join) use ($id)
                {
                    $join->on(DataField::TABLE_NAME.'.'.DataField::ALLOCATION_ID,
                            '=',
                            'client_allocations.AllocationID')
                        ->where('client_allocations.ClientID', $id);
                });
                // ->where(DataField::TABLE_NAME.'.'.DataField::STATUS,'1');
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                // $query->where(DataField::CARRIER_NAME, 'LIKE', '%' . $search . '%');
            });
        }

        if ($request->has('startDate')) {
            $startDate = trim($request->input('startDate'));
            $endDate = trim($request->input('endDate'));
            $query = $query->where(function ($query) use ($startDate, $endDate) {
                $query->whereDate(DataField::TABLE_NAME.'.created_at', '>=', $startDate)
                    ->whereDate(DataField::TABLE_NAME.'.created_at', '<=', $endDate);
            });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);

        $transformedData = $paginated->getCollection()->transform(function($item){
            $item->allocation->StartDate = Carbon::parse($item->allocation->StartDate)->format('M j, Y');
            $item->allocation->EndDate = Carbon::parse($item->allocation->EndDate)->format('M j, Y');

            return $item;
        });

        $transaformedPaginated = new LengthAwarePaginator(
            $transformedData,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), 
            [
                'path' => $request->url(),
                'query' => [ 'page' => $paginated->currentPage() ]
            ]);
            
        return $transaformedPaginated;

    }

    public function listByClient($request){
        $this->SetPage($request);
        $input = $request->all();
        $allocation = $this->clientAllocation->getByClientID($input['id']);
        $query = $this->model::where(DataField::STATUS,'1')
                    ->where(DataField::ALLOCATION_ID, $allocation->AllocationID);
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            // $query = $query->where(function ($query) use ($search) {
            //     $query->where(DataField::CARRIER_NAME, 'LIKE', '%' . $search . '%');
            // });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $result =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy($order_by, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);
        // $result = $result->map(function($item){
        //     $item->date = Carbon::parse($item->created_at)->format('M j');
        //     $item->time = Carbon::parse($item->created_at)->format('g:ia');
        //     $item->productCode = ($item->Credits < 0) ? 'CREDIT ADD' : 'CREDIT ADJUST';
        //     $item->carrier = 'Admin';
        // });

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function create($input){
       $result = null;
       $entity = new Entity;

       $entity->SetData($input);
       $this->model->create($entity->serialize());
    }

    public function update($request , $id){
       $input = $request->all();
       $result= $this->model->where(DataField::ID,$id)->update($input);

       return $result;
    }

    public function save($request){
        $input = $request->all();
        $result = null;
        
        if(!isset($input[Constants::ID])){
           $result = $this->model->create($input);
        }
        else{
           $result = $this->model->where(DataField::ID, $input[Constants::ID])->update($input);
        }

        return $result;
    }

    public function process($input)
    {
        $result = null;
        $entity = new Entity;

        $entity->SetData($input);
        $this->model->create($entity->Serialize());
        return $this->clientAllocation->adjustBudget($entity->AllocationID, $entity->Credits);
    }
}