<?php
namespace Acme\Repositories;

use App\MessageLog;
use Illuminate\Support\Facades\Validator;


class MessageLogRepository extends Repository{
    
    protected $model;
	
	public function __construct()
	{
		$this->model = new MessageLog;
	}

    public function listView(){
        return $this->model->all();
    }

    public function findId($id){
        return $this->model->where('Id', $id)->first();
    }

    public function show($id){
        return $this->model->find($id);
    }

    public function destroy($id){
        $this->model->where('Id', $id)->delete();
    }

    public function save($request){
        $input = $request->all();
        $id = $input['id'];
        
        if($id == null){
            $this->model->create($input);
        }
        else{
            $this->model->where('Id', $id)->update($input);
        }

        return ['status' => true, 'results' => 'Success'];
    }
}