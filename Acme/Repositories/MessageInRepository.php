<?php
namespace Acme\Repositories;

use App\MessageIn as Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

use Acme\Common\DataFields\MessageIn as DataField;
use Acme\Common\Entity\MessageIn as Entity;

use Acme\Common\Constants as Constants;
use Acme\Common\Pagination as Pagination;




class MessageInRepository extends Repository{

    protected $model;

    use Pagination;
	
	public function __construct()
	{
		$this->model = new Model;
	}

     public function getByID($id){
        $result = $this->model->where(DataField::ID, $id)->first();

        return $result;
    }

    public function list($request){
        $this->SetPage($request);
        $query = $this->model->where(DataField::STATUS,'1')
                             ->where(DataField::IS_DELETED,0);
        
        if ($request->has(Constants::KEYWORD)) {
            $search = trim($request->input(Constants::KEYWORD));
            $query = $query->where(function ($query) use ($search) {
                $query->where(DataField::MESSAGE_FROM, 'LIKE', '%' . $search . '%');
            });
        }

        $order_by   = $this->SortBy;
        $sort       = $this->SortOrder;

        $paginated =  $query->select(Constants::SYMBOL_ALL)
            ->orderBy(DataField::ID, $sort)
            ->paginate($this->PageSize,[Constants::SYMBOL_ALL],
                        Constants::PAGE_INDEX,
                        $this->PageIndex);

        $transformedData = $paginated->getCollection()->transform(function($item) {
            return $item;
        });

        $result = new LengthAwarePaginator(
            $transformedData,
            $paginated->total(),
            $paginated->perPage(),
            $paginated->currentPage(), 
            [
                'path' => $request->url(),
                'query' => [ 'page' => $paginated->currentPage() ]
            ]);

        return $result;
    }

    public function show($id){
        $result = $this->model->find($id);

        return $result;
    }

    public function destroy($id){
        $result = $this->model->where(DataField::ID, $id)->delete();

        return $result;
    }

    public function create($entity){
       $result = $this->model->create($entity);
       return $result;
    }

    public function update($entity , $id){
       
       $result = $this->model->where(DataField::ID,$id)->update($entity);

       return $result;
    }

    public function save($entity){
        $result = null;
        
        if($entity[DataField::ID] == ""){
           $result = $this->model->create($entity);
        }
        else{
           $result = $this->model->where(DataField::ID,$entity[DataField::ID])->update($entity);
        }

        return $result;
    }

    public function delete($id)
    {
        $result = null;
        //$result = $this->model->where(DataField::ID, $id)->update([DataField::IS_DELETED => 1]);

        return $result;
    }

    public function getBySendTime($start , $end , $code , $recipient)
    {
        $result = $this->model
                       ->where(DataField::SEND_TIME,">=" ,$start)
                       ->where(DataField::SEND_TIME,"<=", $end)
                       ->where(DataField::MESSAGE_TEXT, 'like', '%'.$code.'%')
                       ->where(DataField::MESSAGE_TEXT, 'like', '%'.$recipient.'%')
                       ->get();

        return $result;
    }

    public function getUnTagged()
    {
        $result = $this->model
                       ->where(DataField::SCANNED,"=" ,0)
                       ->where(DataField::MESSAGE_TEXT, 'like', '%has loaded%')
                       ->get();

        return $result;
    }

    public function updateScanned($id)
    {
        $result = $this->model->where(DataField::ID,$id)->update([DataField::SCANNED => 1]);

        return $result;
    }

}