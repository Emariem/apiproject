// Custom pagination for laravel eloquent pagination results

class LaravelPagination
{
    constructor(data = [], action = null)
    {
        this.generate(data, action);
    }

    generate(data, action)
    {
        this.$ul = $('#'+data.ulId);
        this.pageIndex = data.pageIndex;
        this.pageSize = data.pageSize;
        this.total = (data.total == null) ? 0 : data.total;
        this.dataUrl = data.dataUrl;
        this.prevPageUrl = data.prevPageUrl;
        this.nextPageUrl = data.nextPageUrl;
        this.from = (data.from == null) ? 0 : data.from;
        this.to = (data.to == null) ? 0 : data.to;
        this.action = action;
        this.generatePagination();
    }

    generatePagination()
    {
        this.generateSideControl('prev', this.prevPageUrl);
        this.generatePageControls();
        this.generateSideControl('next', this.nextPageUrl);
        this.attachEvent();
        this.generateLabel();
    }

    generatePageControls()
    {
        var totalPages = Math.ceil(this.total / this.pageSize);
        for(var i = 1; i <= totalPages; i++) {
            this.$ul.append(this.listItem(i));
        }
    }

    listItem(page)
    {
        let url = this.dataUrl + '?page=' + page;
        let active = (this.pageIndex == page) ? 'active' : '';
        let $listItem = '<li class="page-item '+ active +'">'
                        +'<a class="page-link" data-action="' 
                        + this.action
                        +'" data-url="' + url
                        +'" href="#">' + page + '</a></li>';
        return $listItem;
    }

    generateSideControl(type, url)
    {
        let disable = (url ==  null) ? 'lp-link-disabled' : '';
        let $listItem =  '<li class="page-item">'
                        +'<a class="page-link ' + disable
                        +'" data-action="' + this.action
                        +'" data-url="' + url
                        +'" href="#" >' + type + '</a></li>';
        this.$ul.append($listItem);
    }

    generateLabel()
    {
        let label = '<label class="lp-label">Showing '
                    + this.from +' to ' + this.to
                    +' of ' + this.total +' records'
                    +'</label>';
        $(label).insertBefore(this.$ul);
    }

    attachEvent()
    {
        $('.page-link').click(function(event)
        {
            event.preventDefault();
            window[$(this).data('action')]($(this).data('url'));
        });
    }

    destroy()
    {
        $('.lp-label').remove();
        this.$ul.empty();
    }
}