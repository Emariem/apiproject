var module;
$(document).ready(function()
{
	module  = new System.Clients(); // <--- Initialize the module
    module.Load();
    
    $.datepicker.setDefaults( $.datepicker.regional[ "en" ] );
    $("#start-date").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(date) {
            module.CreditHistoryListView();
        }
    }).datepicker('setDate', 'today');

    $("#end-date").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(date) {
            module.CreditHistoryListView();
        }
    }).datepicker('setDate', 'today');

    setTimeout(function()
    {
        module.CreditHistoryListView();
    }, 500);
});

function CreditHistoryListView(url)
{
    module.CreditHistoryListView(url);
}

function BackToTransactions(url)
{
    window.location.href = url;
}

function Remove(id, name)
{
	module.Remove(id);
}

function Initialize(id)
{
	module.Clear();
	module.ClearError();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View(id)
{
	module.Clear();
	// module.DisableFields();
	module.SetReadOnly(true);
	module.Initialize(id);
}