
if (typeof (System) === "undefined") {
    System = {};
};

System.Gateway = function System$Gateway(page) {

	//CONSTANTS -> move to seperate js file
	const FIELD = '.field';
	const REQUIRED = '.vrequired';
	const ID_SELECTOR = '#';
	const LABEL = 'label';
	const ID = 'id';
	const NAME = 'name';
	const USERNAME = 'username';
	const EMAIL = 'email';
	const PASSWORD = 'password';
	const TYPE = 'type';
	const SUCCESS_TITLE = 'Success!';
	const ERROR_TITLE = 'Error!';
	const SUCCESS_TYPE = 'success';
	const ERROR_TYPE = 'error';

	var entity = { 
		id: 'id',
		name: 'name',
		carrier_id: 'carrier_id',
		address: 'address',
		sim: 'sim',
		type: 'type',
		balance: 'balance',
		minimum: 'minimum',
		priority: 'priority',
		status: 'status',
		gateway_id: 'gateway',
		adjust_amount: 'adjust-amount'
	};

	var module = "module";
	var $this = this; 
	var $tips = $(".tips-message");
	var controller = BASE_URL + "gateway";
	
	var $tmpl = $("#list-tmpl");
	var $list = $("#list-result");
	var $table = $("#list-table");

	var $pagination = new LaravelPagination();
	
	var inputs = {} ;
	var $fields;

    var $save_btn = $('#save-btn');

	this.Load = function System$Load() 
	{
		var $temp = $();
		$(FIELD).each(function(){ 
			$temp = $temp.add( $(this) ); 
		});
		$this.$fields = $temp;
	}
	
	this.Validate = function System$$Validate() 
	{
		var retVal = true;
		$this.ClearError();
		
		//VALIDATION STARTS HERE
        $this.$fields.filter(REQUIRED).each(function(){
            Validate($tips, $(this), $(this).attr(LABEL)+ " is required");
        });
		
		return retVal;
	}
	
	this.ListView = function System$ListView(url = null)
	{
		let data = {}
		let action = Common.Request
		url = (url == null) ? controller + '/list' : url;

		action(url, data, "GET", function(res){
			let result = res.data.data
			var list = result.data;

			var $templateMain = $tmpl.tmpl(list);
			$list.html($templateMain);

			$pagination.destroy();
			$pagination.generate({
				ulId: "gateway-pagination",
				pageIndex: result.current_page,
				pageSize: result.per_page,
				total: result.total,
				prevPageUrl: result.prev_page_url,
				nextPageUrl: result.next_page_url,
				from: result.from,
				to: result.to,
				dataUrl: result.path
			}, 'ListView');
		})
	}

	this.KeyValue = function System$KeyValue(elem)
	{
		let data = {}
		let action = Common.Request
		var $elem = $('#'+elem);
		action(controller + '/keyvalue', data, "GET", function(res){
			let result = res.data.data
			
			$elem.empty();
			for(var item in result) {
				$elem.append('<option value="'+item+'">'+result[item]+'</option>');
			}
		})
	}

	this.AdjustBalance = function System$AdjustBalance(type)
	{
		let action = Common.Request
		let data = $this.GetFieldValues([
            entity.gateway_id,
            entity.adjust_amount
		]);
		//manipulate data here 
		data['id'] = data['gateway'];
		if(type == 'remove') {
			data['adjust-amount'] = data['adjust-amount'] * (-1);
		}

		action(controller + '/adjustbalance', data, "POST", function(res)
        {
			let result = res.data

			closeModal("adjust-"+module+"-modal");
			if(!result.error){
				$this.ListView();
				Dialog(SUCCESS_TITLE, result.message, SUCCESS_TYPE);
			}
			else{
				Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}
		});
	}

	this.Initialize = function System$Initialize(id) 
	{
		let data = {};
		let action = Common.Request

		action(controller+"/initialize/"+ id, data, "GET", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$this.$fields.each(function(){
					$(this).val(list[$(this).attr(ID).toLowerCase()]).trigger('change');
				});
				openModal(module+'-modal');
			}
			else{
				 Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}	
		});
	}
	
	this.Save = function System$Save()
	{
		if(!$this.Validate()){
			return;
		}
		
		$save_btn.attr("disabled","disabled");
		let action = Common.Request
		let data = $this.GetFieldValues([
			entity.id,
            entity.name,
            entity.carrier_id,
            entity.address,
            entity.sim,
            entity.type,
            entity.balance,
            entity.minimum,
            entity.priority,
            entity.status
		]);
		//manipulate data here 


		action(controller + '/save', data, "POST", function(res)
        {
			$save_btn.removeAttr("disabled");
			let result = res.data

			if(!result.error){
				closeModal(module+"-modal");
				$this.ListView();
				Dialog(SUCCESS_TITLE, result.message, SUCCESS_TYPE);
			}
			else{
				Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}
		});
	}
	
	this.Remove = function System$Remove(id, name)
	{	
		let data = { id: id};
		let action = Common.Request

		Dialog('Delete Confirmation', 
		'Are your sure you want to delete ' + name + ' ?', 
		'warning',
		true,
		'Yes',
		function(isConfirm){
			if(isConfirm.value) {
				action(controller + "/delete",
				data,
				"POST",
				function(res){
					let result = res.data

					if(!result.error){
						Dialog("Deleted!", name +" has been deleted.", "success");
						$this.ListView();
					}
					else{
						Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
					}
				});
			} else {
				Dialog("Cancelled", "Cancelled", "error");
			}
		});
	}
	
	this.GetFieldValues = function System$GetFieldValues(columns)
	{
		let data = {};
		for(var i in columns){
			data[columns[i]] = $this.$fields.filter(ID_SELECTOR + columns[i]).val();
		}
		return data;
	}

	this.GetListUrl = function System$GetListUrl(url, type, id = null)
	{
		if(type == 'next') {
			url = $page_next.attr('data-id');
		} else if(type == 'prev'){
			url = $page_prev.attr('data-id');
		}

		if(typeof(url) != 'undefined') {
			url = (type != null) ? url+'&id='+id : ((id != null) ? url+'?id='+id : url);
		}

		return url;
	}
	
	this.Clear = function System$Clear()
	{
		$this.DisableFields(false);
		$this.ClearError();
		$this.$fields.val("");
	}
	
	this.ClearError = function System$ClearError()
	{
		$this.$fields.removeClass("input-error");
		$tips.html("").hide();
	}

	this.SetReadOnly = function System$SetReadOnly(value)
	{
		$this.$fields.attr('disabled', value);
	}
	
	this.DisableFields = function System$DisableFields(isTrue)
	{
		$this.ClearError();
		$this.$fields.attr('disabled',isTrue);
	}

	this.GeneratePassword =  function System$GeneratePassword()
	{
		var password = Math.random().toString(36).slice(-8);
		$this.$fields.filter(ID_SELECTOR+PASSWORD).val(password).trigger('change');
	}
}
