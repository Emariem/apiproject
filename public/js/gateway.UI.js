var module;
$(document).ready(function()
{
	module  = new System.Gateway(); // <--- Initialize the module
	module.ListView();
    module.Load();
	
	$("#save-btn").click(function(){
		module.Save();
	});

	$('#add-btn').click(function(){
		module.AdjustBalance('add');
	});

	$('#remove-btn').click(function(){
		module.AdjustBalance('remove');
	});
	
});

function Remove(id, name)
{
	module.Remove(id, name);
}

function Initialize(id)
{
	module.Clear();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View()
{
	module.Initialize();
}

function ListView(url)
{
	module.ListView(url);
}

function AddModuleForm()
{
	module.Clear();

	$("#carrier_id").val($("#carrier_id option:first").val());
	$("#type").val($("#type option:first").val());
	$("#status").val($("#status option:first").val());
	openModal('module-modal');
}

function AdjustModuleForm()
{
	$('#adjust-amount').val(0).trigger('change');
	module.KeyValue('gateway');
	openModal('adjust-module-modal');
}
