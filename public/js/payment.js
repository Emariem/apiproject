
if (typeof (System) === "undefined") {
    System = {};
};

System.Payment = function System$Payment(page) {

	//CONSTANTS -> move to seperate js file
	const FIELD = '.field';
	const REQUIRED = '.vrequired';
	const ID_SELECTOR = '#';
	const LABEL = 'label';
	const ID = 'id';

	var entity = { 
        id: 'id',
        allocation_id: 'allocation-id',
		date: 'date',
		amount: 'amount',
	};

	var module = "payment";
	var $this = this; 
	var $tips = $(".tips-message");
	var controller = BASE_URL + "payment";
	
	var $tmpl = $("#list-tmpl");
	var $list = $("#list-result");
	var $table = $("#list-table");

	var $start_date = $('#start-date');
	var $end_date = $('#end-date');

	var $pagination = new LaravelPagination();

    var $client_id = $('#client-id');
	var inputs = {} ;
	var $fields;

	var $save_btn = $('#save-payment-btn');
	

	this.Load = function System$Load() 
	{
		var $temp = $();
		$(FIELD).each(function(){ 
			$temp = $temp.add( $(this) ); 
		});
        $this.$fields = $temp;
	}
	
	this.Validate = function System$$Validate() 
	{
		var retVal = true;
		$this.ClearError();
		
		//VALIDATION STARTS HERE
        $this.$fields.filter(REQUIRED).each(function(){
            Validate($tips, $(this), $(this).attr(LABEL)+ " is required");
        });
		
		return retVal;
	}
	
	this.ListView = function System$ListView()
	{
		let data = {}
        let action = Common.Request;
        let id = $client_id.val();
        let url = controller + '/list?id=' + id;

		action(url, data, "GET", function(res){
			let result = res.data;
			var list = result.data.data;

			var $templateMain = $tmpl.tmpl(list);
			$list.html($templateMain);
			$this.Pagination(result, 'ListView');
		})

	}
	
	this.Pagination = function System$Pagination(data, action)
	{
		$pagination.destroy();
		$pagination.generate({
			ulId: "pagination-content",
			pageIndex: data.current_page,
			pageSize: data.per_page,
			total: data.total,
			prevPageUrl: data.prev_page_url,
			nextPageUrl: data.next_page_url,
			from: data.from,
			to: data.to,
			dataUrl: data.path
		}, action);
	}
    
	this.Initialize = function System$Initialize(id) 
	{
		let data = {};
		let action = Common.Request

		action(controller+"/initialize/"+ id, data, "GET", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$this.$fields.each(function(){
					$(this).val(list[$(this).attr(ID).toLowerCase()]).trigger('change');
				});
				openModal(module+'-modal');
			}
			else{
				 alert(result.message);
			}	
		});
	}
	
	this.Save = function System$Save()
	{
		if(!$this.Validate()){
			return;
		}
		
		$save_btn.attr("disabled","disabled");
		let action = Common.Request
		let data = $this.GetFieldValues([
			entity.id,
			entity.allocation_id,
			entity.date,
			entity.amount,
        ]);
		//manipulate data here 
		action(controller + "/save", data, "POST", function(res)
        {
			$save_btn.removeAttr("disabled");
			let result = res.data

			if(!result.error){
				closeModal(module+"-modal");
				Dialog(SUCCESS_TITLE, result.message, SUCCESS_TYPE);
			}
			else{
				Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}
		});
	}
	
	this.Remove = function System$Remove(id, name)
	{	
		let data = { id: id};
		let action = Common.Request

		Dialog('Delete Confirmation', 
		'Are your sure you want to delete ' + name + ' ?', 
		'warning',
		true,
		'Yes',
		function(isConfirm){
			if(isConfirm.value) {
				action(controller + "/delete",
				data,
				"POST",
				function(res){
					let result = res.data

					if(!result.error){
						Dialog("Deleted!", name +" has been deleted.", "success");
						$this.ListView();
					}
					else{
						Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
					}
				});
			} else {
				Dialog("Cancelled", "Cancelled", "error");
			}
		});
	}

	this.GetFieldValues = function System$GetFieldValues(columns)
	{
		let data = {};
		for(var i in columns){
			data[columns[i]] = $this.$fields.filter(ID_SELECTOR + columns[i]).val();
		}
		return data;
	}

	this.Clear = function System$Clear()
	{
		$this.DisableFields(false);
		$this.ClearError();
		$this.$fields.val("");
		$this.$fields.filter(ID_SELECTOR + 'amount').val(0);
	}
	
	this.ClearError = function System$ClearError()
	{
		$this.$fields.removeClass("input-error");
		$tips.html("").hide();
	}

	this.SetReadOnly = function System$SetReadOnly(value)
	{
		$this.$fields.attr('disabled', value);
	}
	
	this.DisableFields = function System$DisableFields(isTrue)
	{
		$this.ClearError();
		$this.$fields.attr('disabled',isTrue);
	}

	this.setDefaultDate  = function System$setDefaultDate(callback)
	{
		let start_date = moment().format("YYYY-MM-DD");
		let end_date = moment().format("YYYY-MM-DD");

		$start_date.val(start_date);
		$end_date.val(end_date);

		if(typeof(callback) == "function")
		{
			callback(true);
		}
	}

}
