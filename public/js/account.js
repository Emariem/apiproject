
if (typeof (System) === "undefined") {
    System = {};
};

System.Account = function System$Account(page) {

	//CONSTANTS -> move to seperate js file
	const FIELD = '.field';
	const REQUIRED = '.vrequired';
	const ID_SELECTOR = '#';
	const LABEL = 'label';
	const ID = 'id';
	const NAME = 'name';
	const USERNAME = 'username';
	const EMAIL = 'email';
	const PASSWORD = 'password';
	const TYPE = 'type';
	const SUCCESS_TITLE = 'Success!';
	const ERROR_TITLE = 'Error!';
	const SUCCESS_TYPE = 'success';
	const ERROR_TYPE = 'error';

	var entity = { 
		id: 'id',
		username: 'username',
		email: 'email',
		password: 'password',
		password_confirmation: 'password_confirmation',
		type: 'type',
		is_active: 'is_active',
	};

	var module = "user";
	var $this = this; 
	var $tips = $(".tips-message");
	var controller = BASE_URL + "account";
	
	// var $tmpl = $("#list-tmpl");
	// var $list = $("#list-result");
	// var $table = $("#list-table");

	// var $transaction_tmpl = $("#transactions-list-tmpl");
	// var $transaction_list = $("#transactions-list-result");
	// var $transaction_table = $("#transactions-list-table");
	
	var inputs = {} ;
	var $fields;

    var $save_btn = $('#save-btn');

	this.Load = function System$Load() 
	{
		var $temp = $();
		$(FIELD).each(function(){ 
			$temp = $temp.add( $(this) ); 
		});
		$this.$fields = $temp;
		console.log($this.$fields);
	}
	
	this.Validate = function System$$Validate() 
	{
		var retVal = true;
		$this.ClearError();
		
		//VALIDATION STARTS HERE
        $this.$fields.filter(REQUIRED).each(function(){
            Validate($tips, $(this), $(this).attr(LABEL)+ " is required");
        });
		
		return retVal;
	}
	
	this.ListView = function System$ListView()
	{
		let data = {}
		let action = Common.Request

		action(controller +'/list', data, "GET", function(res){
			let result = res.data
			var list = result.data;
			// $("#"+module+"_table").dataTable().fnDestroy();
			var $templateMain = $tmpl.tmpl(list);
			$list.html($templateMain);
		})

	}

	this.ClientTransactionsListView = function System$ClientTransactionsListView()
	{
		let data = $this.GetFieldValues([entity.id]);
		let action = Common.Request

		action(BASE_URL +'transactions/listbyclientallocation', data, "POST", function(res){
			let result = res.data
			var list = result.data;
			var page_current = result.current_page; 
			var $templateMain = $transaction_tmpl.tmpl(list);
			$transaction_list.html($templateMain);
		})

	}

	this.Initialize = function System$Initialize(id) 
	{
		let data = {};
		let action = Common.Request

		action(controller+"/initialize/"+ id, data, "GET", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$this.$fields.each(function(){
					$(this).val(list[$(this).attr(ID).toLowerCase()]).trigger('change');
				});
				openModal(module+'-modal');
			}
			else{
				 alert(result.message);
			}	
		});
	}
	
	this.Save = function System$Save()
	{
		if(!$this.Validate()){
			return;
		}
		
		$save_btn.attr("disabled","disabled");
		let action = Common.Request
		let data = $this.GetFieldValues([
			entity.id,
			entity.CompanyName,
			entity.username,
			entity.email,
			entity.type,
			entity.is_active,
			entity.password,
			entity.password_confirmation
		]);

		action(controller + "/save", data, "POST", function(res)
        {
			$save_btn.removeAttr("disabled");
			let result = res.data

			if(!result.error){
				closeModal(module+"-modal");
				$this.Clear();
				Dialog(SUCCESS_TITLE, result.message, SUCCESS_TYPE);
			}
			else{
				Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}
		});
	}
	
	this.Remove = function System$Remove(id, name)
	{	
		let data = { id: id};
		let action = Common.Request

		Dialog('Delete Confirmation', 
		'Are your sure you want to delete ' + name + ' ?', 
		'warning',
		true,
		'Yes',
		function(isConfirm){
			if(isConfirm.value) {
				action(controller + "/delete",
				data,
				"POST",
				function(res){
					let result = res.data

					if(!result.error){
						Dialog("Deleted!", name +" has been deleted.", "success");
						$this.ListView();
					}
					else{
						Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
					}
				});
			} else {
				Dialog("Cancelled", "Cancelled", "error");
			}
		});
	}

	this.GetCredentials = function System$GetCredentials(id) 
	{
		let data = { id: id };
		let action = Common.Request

		action(controller+"/getcredentials", data, "POST", function(res){
			let result = res.data
			var list = result.data;
			
			if(!result.error && list.length > 0){
				list = list[0];
				$this.$fields.filter(ID_SELECTOR + credentials.ClientID).val(list.id).trigger('change');
				$this.$fields.filter(ID_SELECTOR + credentials.ClientSecret).val(list.secret).trigger('change');
				openModal(module + '-credentials-modal');
			}
			else{
				 alert(result.message);
			}	
		});
	}

	this.GetFieldValues = function System$GetFieldValues(columns)
	{
		let data = {};
		for(var i in columns){
			data[columns[i]] = $this.$fields.filter(ID_SELECTOR + columns[i]).val();
		}
		return data;
	}
	
	this.Clear = function System$Clear()
	{
		$this.DisableFields(false);
		$this.ClearError();
		$this.$fields.filter('#password').val("");
		$this.$fields.filter('#password_confirmation').val("");
		// $this.$fields.val("");
	}
	
	this.ClearError = function System$ClearError()
	{
		$this.$fields.removeClass("input-error");
		$tips.html("").hide();
	}

	this.SetReadOnly = function System$SetReadOnly(value)
	{
		$this.$fields.attr('disabled', value);
	}
	
	this.DisableFields = function System$DisableFields(isTrue)
	{
		$this.ClearError();
		$this.$fields.attr('disabled',isTrue);
	}

	this.GeneratePassword =  function System$GeneratePassword()
	{
		var password = Math.random().toString(36).slice(-8);
		$this.$fields.filter(ID_SELECTOR+PASSWORD).val(password).trigger('change');
	}
}
