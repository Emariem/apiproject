var module;
$(document).ready(function()
{
	module  = new System.Account(); // <--- Initialize the module
	// module.ListView();
    module.Load();

    $("#save-btn").click(function(){
		module.Save();
	});
	
});

function Remove(id, name)
{
	module.Remove(id, name);
}

function Initialize(id)
{
	module.Clear();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function View(url)
{
	window.location.href = url;
}

function ViewCredentials(id)
{
	module.GetCredentials(id);
}

function AddModuleForm()
{
	module.Clear();
	openModal('client-modal');
}

function GeneratePassword()
{
	module.GeneratePassword();
}

function TogglePassword()
{
	var $password = $('#password');
	let type = $password.attr('type');
	type = ( type == 'text' ) ? 'password' : 'text';
	$password.attr('type', type);
}
