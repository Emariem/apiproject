
if (typeof (System) === "undefined") {
    System = {};
};

System.ProductCode = function System$ProductCode(page) {

	//CONSTANTS -> move to seperate js file
	const FIELD = '.field';
	const REQUIRED = '.vrequired';
	const ID_SELECTOR = '#';
	const LABEL = 'label';
	const ID = 'id';

	const TYPE = 'type';
	const SUCCESS_TITLE = 'Success!';
	const ERROR_TITLE = 'Error!';
	const SUCCESS_TYPE = 'success';
	const ERROR_TYPE = 'error';

	var entity = { 
		id: 'id',
		CarrierID : "carrier_id",
		RegisterTo : "register_to",
		Code : "code",
		Name: "name",
		USSDTemplate: "ussd_template",
        Amount : "amount",
        AmountCharged : "amount_charged",
        Status : "status"
	};


	var module = "productcode";
	var $this = this; 
	var $tips = $(".tips-message");
	var controller = BASE_URL + "productcode";
	
	var $tmpl = $("#list-tmpl");
	var $list = $("#list-result");
	var $table = $("#list-table");

	var $pagination = new LaravelPagination();

	var inputs = {} ;
	var $fields;

    var $save_btn = $('#save-btn');

	this.Load = function System$Load() 
	{
		var $temp = $();
		$(FIELD).each(function(){ 
			$temp = $temp.add( $(this) ); 
		});
		$this.$fields = $temp;
	}
	
	this.Validate = function System$$Validate() 
	{
		var retVal = true;
		$this.ClearError();
		
		//VALIDATION STARTS HERE
        $this.$fields.filter(REQUIRED).each(function(){
            Validate($tips, $(this), $(this).attr(LABEL)+ " is required");
        });
		
		return retVal;
	}
	
	this.ListView = function System$ListView(url = null)
	{
		let data = {
			Search : $("#search").val(),
			CarrierID : $("#search_carrier").val()
		}

		let action = Common.Request
		url = (url == null) ? controller + '/list' : url;

		action(url, data, "POST", function(res){
			let result = res.data.data
			var list = result.data;
		
			// $("#"+module+"_table").dataTable().fnDestroy();
			var $templateMain = $tmpl.tmpl(list);
			$list.html($templateMain);

			$pagination.destroy();
			$pagination.generate({
				ulId: "productcode-pagination",
				pageIndex: result.current_page,
				pageSize: result.per_page,
				total: result.total,
				prevPageUrl: result.prev_page_url,
				nextPageUrl: result.next_page_url,
				from: result.from,
				to: result.to,
				dataUrl: result.path
			}, 'ListView');
		})

	}

	this.Initialize = function System$Initialize(id) 
	{
		let data = {};
		let action = Common.Request

		action(controller+"/initialize/"+ id, data, "GET", function(res){
			let result = res.data
			var list = result.data;

			if(!result.error){
				$this.$fields.each(function(){
					$(this).val(list[$(this).attr(ID).toLowerCase()]).trigger('change');
				});
				openModal('module-modal');
			}
			else{
				 alert(result.message);
			}	
		});
	}
	
	this.Save = function System$Save()
	{
		if(!$this.Validate()){
			return;
		}
		
		$save_btn.attr("disabled","disabled");
		let action = Common.Request
		let data = $this.GetFieldValues([
			entity.id,
			entity.CarrierID,
            entity.RegisterTo,
			entity.Code,
			entity.Name,
			entity.USSDTemplate,
            entity.Amount,
            entity.AmountCharged
		]);

		data[entity.Status] = 1;

		action(controller + "/save", data, "POST", function(res)
        {
			$save_btn.removeAttr("disabled");
			let result = res.data

			if(!result.error){
				closeModal("module-modal");
				$this.ListView();
				Dialog(SUCCESS_TITLE, result.message, SUCCESS_TYPE);
			}
			else{
				Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
			}
		});
	}
	
	this.Remove = function System$Remove(id, name)
	{	
		let data = { id: id};
		let action = Common.Request

		Dialog('Delete Confirmation', 
		'Are your sure you want to delete ' + name + ' ?', 
		'warning',
		true,
		'Yes',
		function(isConfirm){
			if(isConfirm.value) {
				action(controller + "/delete",
				data,
				"POST",
				function(res){
					let result = res.data

					if(!result.error){
						Dialog("Deleted!", name +" has been deleted.", "success");
						$this.ListView();
					}
					else{
						Dialog(ERROR_TITLE, result.message, ERROR_TYPE);
					}
				});
			} else {
				Dialog("Cancelled", "Cancelled", "error");
			}
		});
	}

	this.GetFieldValues = function System$GetFieldValues(columns)
	{
		let data = {};
		for(var i in columns){
			data[columns[i]] = $this.$fields.filter(ID_SELECTOR + columns[i]).val();
		}
		return data;
	}

	this.GetListUrl = function System$GetListUrl(url, type, id = null)
	{
		if(type == 'next') {
			url = $page_next.attr('data-id');
		} else if(type == 'prev'){
			url = $page_prev.attr('data-id');
		}

		if(typeof(url) != 'undefined') {
			url = (type != null) ? url+'&id='+id : ((id != null) ? url+'?id='+id : url);
		}

		return url;
	}
	
	this.Clear = function System$Clear()
	{
		$this.DisableFields(false);
		$this.ClearError();
		$this.$fields.val("");
	}
	
	this.ClearError = function System$ClearError()
	{
		$this.$fields.removeClass("input-error");
		$tips.html("").hide();
	}

	this.SetReadOnly = function System$SetReadOnly(value)
	{
		$this.$fields.attr('disabled', value);
	}
	
	this.DisableFields = function System$DisableFields(isTrue)
	{
		$this.ClearError();
		$this.$fields.attr('disabled',isTrue);
	}

}
