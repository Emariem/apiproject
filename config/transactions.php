<?php 

return [
    'type' => [
        '0' => 'CREDIT ADD',
        '1' => 'CREDIT REMOVE',
        '3' => 'LOAD REQUEST',
        '4' => 'ADJUSTMENT',
        '5' => 'INITIAL CREDITS',
        '6' => 'REFUNDS'
    ],
     'status' => [
        '0' => 'Sent',
        '1' => 'Success',
        '2' => 'Failed',
        '4' => 'Adjustment',
        '5' => 'System',
        '6' => 'Refunds'
    ],
];