

export function Request(url , data , method, callback) {

    let list = {}

    switch(method)
    {
        case "POST" : list =  axios.post(url,data); break;
        case "GET" : list = axios.get(url,data); break;
    }

    list.then(res => {

         let error = false
         let data = []
         let message = ""
         let tags = {}
         let errorCodes = ["0000"]

         if(typeof(res.data) != "object")
         {
             data  = []
             error = true
             message = "Invalid JSON Format."
             tags = {}
             errorCodes = [999]

         }
         else
         {
            data = res.data.data
            message = res.data.message
            tags = res.data.tags
            error = res.data.error
            errorCodes = res.data.errorCodes


            if(errorCodes.length != 0)
            {
                message = parseInt(errorCodes[0]) == 500? 
                    "The system encountered an error. Please try again in a few minutes or contact the System Administrator."
                    : message; 
            }
         }

         let result = {
            data : {
                data : data,
                message : message,
                error : error,
                tags : tags,
                errorCodes :errorCodes
            }
         }

         if(typeof(callback)== "function")
         {
             callback(result)
         }
    })
    .catch(err => {
         let result = {
            data : {
                data : null,
                message : "The system encountered an error. Please try again in a few minutes or contact the System Administrator.",
                error : true,
                tags : 0,
                errorCodes :[500]
            }
         }

         if(typeof(callback)== "function")
         {
             callback(result)
         }
    });
    
}

export function Validate($tips, $elements , message)
{
    if ($.trim($elements.val()) == "" || $.trim($elements.val()) == "null")
    {
        //$elements.addClass('pjhcodes-error');
        $tips.show();
        $elements.focus();
        $tips.html(message);
        throw new Error('This is not an error. This is just to abort javascript');
    }
}

export function log(title,message,type)
{
    console.log(title,message)
}

export function openModal(id)
{
    $('#'+id).modal('show');
}

export function closeModal(id)
{
    $('#'+id).modal('hide');
}

export function TestCommon()
{
    alert(1)
}

export function Dialog(title, message, type, showCancelButton = false,  confirmButtonText = '', callback = null)
{
    if(confirmButtonText != '') {
        Swal.fire({
            title: title,
            text: message,
            type: type,
            showCancelButton: showCancelButton,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmButtonText
        }).then((result) => callback(result));
    } else {
        Swal.fire(title, message, type);
    }
}