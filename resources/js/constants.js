export const SUCCESS_TITLE = 'Success!';
export const ERROR_TITLE = 'Error!';
export const SUCCESS_TYPE = 'success';
export const ERROR_TYPE = 'error';
