
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
const common = require('./common');
const constants = require('./constants');

// import Vue from "vue";
// import Vuetify from 'vuetify';
// import $ from 'jquery';
// import tmpl from 'jquery.tmpl';
import Swal from 'sweetalert2';
import datepickerFactory from 'jquery-datepicker';
import datepickerJAFactory from 'jquery-datepicker/i18n/jquery.ui.datepicker-ja';
import moment from 'moment';
 
// Just pass your jquery instance and you're done
datepickerFactory($);
datepickerJAFactory($);

window.$ = window.jQuery = $;

window.Swal = Swal;
window.Common = common
window.Validate = common.Validate
window.openModal = common.openModal
window.closeModal = common.closeModal
window.Dialog = common.Dialog
window.Common = common

window.SUCCESS_TITLE = constants.SUCCESS_TITLE
window.ERROR_TITLE = constants.ERROR_TITLE
window.SUCCESS_TYPE = constants.SUCCESS_TYPE
window.ERROR_TYPE = constants.ERROR_TYPE

window.Constants = constants
window.moment = moment

// Vue.use(Vuetify);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Vue.component(
//     'passport-clients',
//     require('./components/passport/Clients.vue').default
// );

// Vue.component(
//     'passport-authorized-clients',
//     require('./components/passport/AuthorizedClients.vue').default
// );

// Vue.component(
//     'passport-personal-access-tokens',
//     require('./components/passport/PersonalAccessTokens.vue').default
// );

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });
