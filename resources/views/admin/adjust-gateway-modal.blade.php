<div class="modal in" id="adjust-module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Adjust Gateway Balance</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group bmd-form-group">
                    {!! Form::select('gateway', [], null, ['id' => 'gateway', 'class' => 'form-control field ', 'label' => 'Gateway']) !!}
                </div>
                <div class="form-group bmd-form-group">
                    {!! Form::label('balance-lbl', 'Enter Amount', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                    {!! Form::number('adjust-amount', 0, ['id' => 'adjust-amount', 'class' => 'form-control field ', 'label' => 'Amount']) !!}
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="add-btn">Add</button>
            <button type="button" class="btn btn-primary" id="remove-btn">Remove</button>
            </div>
        </div>
        </div>
    </div>