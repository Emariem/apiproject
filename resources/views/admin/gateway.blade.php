@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title ">Gateways</h4>
                            {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                            <!-- <div class="custom-card-wrapper">
                                <div class="custom-card">
                                    <div class="card-header">
                                        <p><strong> Total Credits Loaded to All</strong></p>
                                        <p class="right-align no-margin">10,000</p>
                                    </div>
                                </div>
                                <div class="custom-card">
                                    <div class="card-header">
                                        <p><strong> Total Successful Transaction</strong></p>
                                        <p class="right-align no-margin">10,000</p>
                                    </div>
                                </div>
                                <div class="custom-card">
                                    <div class="card-header">
                                        <p><strong> Total Failed Transaction</strong></p>
                                        <p class="right-align no-margin">10,000</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="card-body">
                            <button type="button" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="AddModuleForm()">
                                    <span class="btn-label">Add Gateway</span>
                            </button>
                            <button type="button" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="AdjustModuleForm()">
                                    <span class="btn-label">Adjust Balance</span>
                            </button>
                            <div class="table-responsive">
                                <table id="list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Name</th>
                                    <th>SIM #</th>
                                    <th>Balance</th>
                                    <th>Minimun</th>
                                    <th>Carrier</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th width="8%">Action</th>
                                    </thead>
                                    <tbody id="list-result"></tbody>
                                </table>
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="gateway-pagination" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@include('admin.add-gateway-modal')
@include('admin.adjust-gateway-modal')
@endsection
@section('scripts')
    <script src="{{ asset('js/gateway.js') }}"></script>
    <script src="{{ asset('js/gateway.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="list-tmpl">
        <tr>
            <td>${ Name }</td>
            <td>${ SIM }</td>
            <td>${ Balance }</td>
            <td>${ Minimum }</td>
            <td>${ Carrier }</td>
            <td>${ Priority }</td>
            <td>${ Status }</td>
            <td>
                <div class="btn-group">
                    <button type="button" data-id="${ id }" title="Edit" class="btn custom-btn btn-raised btn-primary btn-sm" onClick="Initialize(${ GatewayID })"><i class="fa fa-edit"></i></button>
                    <button type="button" data-id="${ id }" title="Remove" class="btn custom-btn btn-raised btn-danger btn-sm" onClick="Remove(${ GatewayID }, '${ Name }')"><i class="fa fa-times"></i></button>
                </div>  
            </td>
        </tr>
    </script>
@endsection