@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                                <h4 class="card-title ">Allocations - {{ $client->info->CompanyName }}</h4>
                        </div>
                        <div class="card-body">
                            {{ Form::open([ 'class' => 'form-inline']) }}
                                {{ Form::hidden('client-id', $client->id, ['id' => 'client-id', 'class' => 'field' ])}}
                                {{-- {{ Form::label('Start Date') }}
                                {{ Form::text('dates', null, [ 'id' => 'start-date', 'class' => 'field form-control half-width left-indent', 'readonly' => true]) }}
                                {{ Form::label('End Date', '', ['class' => 'left-indent']) }}
                                {{ Form::text('dates', null, [ 'id' => 'end-date', 'class' => 'field form-control half-width left-indent', 'readonly' => true]) }} --}}
                            {{ Form::close() }}
                        <button type="button" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="GoToPaymentHistory('{{ route('payment.showbyclient', $client->id) }}');">
                                <span class="btn-label">View Payment History</span>
                            </button>
                            </br>
                            <div class="table-responsive">
                                <table id="list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Interval</th>
                                    <th>Budget</th>
                                    <th>Consumed</th>
                                    <th>Amount Paid</th>
                                    <th>Payment Status</th>
                                    <th width="18%">Action</th>
                                    </thead>
                                    <tbody id="list-result"></tbody>
                                </table>
                                
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="pagination-content" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@include('admin.allocation.add-payment-modal')
@endsection
@section('scripts')
    <script src="{{ asset('js/payment.js') }}"></script>
    <script src="{{ asset('js/allocation.js') }}"></script>
    <script src="{{ asset('js/allocation.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="list-tmpl">
        <tr>
            <td>${ StartDate } - ${ EndDate }</td>
            <td>${ Budget }</td>
            <td>${ Consumed }</td>
            <td>${ TotalPaid }</td>
            <td>${ PaymentStatus }</td>
            <td>
                @{{if PaymentStatus != 'Paid' }}
                    <button type="button" style="float:left;" class="btn btn-labeled btn-sm right-align custom-primary-btn" onClick="AddPaymentForm(${ AllocationID }, '${ StartDate } - ${ EndDate }')" data-toggle="modal">
                        <span class="btn-label">Add payment</span>
                    </button>
                @{{/if}}
            </td>
        </tr>
    </script>
@endsection