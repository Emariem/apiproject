@extends('admin.layout.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                                <h4 class="card-title ">Credit History - {{ $client->info->CompanyName }}</h4>
                        </div>
                        <div class="card-body">
                            {{ Form::open([ 'class' => 'form-inline']) }}
                                {{ Form::hidden('id', $client->id, ['id' => 'id', 'class' => 'field' ])}}
                                {{ Form::label('Start Date') }}
                                {{ Form::text('dates', null, [ 'id' => 'start-date', 'class' => 'field form-control half-width left-indent', 'readonly' => true]) }}
                                {{ Form::label('End Date', '', ['class' => 'left-indent']) }}
                                {{ Form::text('dates', null, [ 'id' => 'end-date', 'class' => 'field form-control half-width left-indent', 'readonly' => true]) }}
                            {{ Form::close() }}
                            </br>
                            <div class="table-responsive">
                                <table id="transactions-list-table" class="table custom-table">
                                    <thead class=" text-primary">
                                    <th>Interval</th>
                                    <th>Adjustment (Credits)</th>
                                    <th>Date</th>
                                    </thead>
                                    <tbody id="history-list-result"></tbody>
                                </table>
                                
                                <nav id="pagination" aria-label="Client table navigation">
                                    <ul id="client-pagination" class="pagination justify-content-end lp-pagination">
                                    </ul> 
                                </nav>
                                <button type="button" style="float:left;" class="btn btn-labeled btn-sm right-align custom-primary-btn" data-toggle="modal" onClick="BackToTransactions('{{ route('admin.client.show', [$client->id]) }}')">
                                        <span class="btn-label">Go Back to Client Transactions</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
<!-- Modal -->
@endsection
@section('scripts')
    <script src="{{ asset('js/clients.js') }}"></script>
    <script src="{{ asset('js/client-credit-history.UI.js') }}"></script>
@endsection
@section('jquery-tmpl')
    <script type="text/x-jQuery-tmpl" id="history-list-tmpl">
        <tr>
            <td>${ allocation.StartDate } - ${ allocation.EndDate }</td>
            <td>
                @{{if Credits < 0 }}
                    <span style="color:red">
                @{{else}}
                    <span style="color:green">
                @{{/if}}
                ${Credits}</span>
            </td>
            <td>${ created_at }</td>
        </tr>
    </script>
@endsection