<div class="modal in" id="client-credentials-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Client Credentials</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group bmd-form-group full-width">
                    {!! Form::label('ClientID-lbl', 'CLIENT ID', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                    {!! Form::text('ClientID', '', [ 'class' => 'form-control full-width field', 'disabled' => true, 'id' => 'ClientID']) !!}

                </div>
                <div class="form-group bmd-form-group full-width">
                        {!! Form::label('ClientSecret-lbl', 'CLIENT SECRET', [ 'class' => 'bmd-label-floating left-indent' ]) !!}
                        {!! Form::text('ClientSecret', '', [ 'class' => 'form-control full-width field', 'disabled' => true, 'id' => 'ClientSecret']) !!}
                </div>
            </div>
            <div class="modal-footer">
            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save-btn">Save changes</button> --}}
            </div>
        </div>
        </div>
    </div>