<div class="sidebar custom-navigation" data-color="purple" data-background-color="black" data-image="../assets/img/sidebar-2.jpg">
    <div class="logo">
    <a href="#" class="simple-text logo-normal">
        SMS Gateway
    </a>
    </div>
    <div class="sidebar-wrapper">
    <ul class="nav">
       @if( Auth::user()->type == 1)
        <li class="nav-item ">
        <a class="nav-link" href="{{ route('admin.clients') }}">
            {{-- <i class="material-icons">content_paste</i> --}}
            <p>Clients</p>
        </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('admin.gateway.index') }}">
                {{-- <i class="material-icons">content_paste</i> --}}
                <p>Gateways</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('admin.productcode') }}">
                {{-- <i class="material-icons">{{ route('account.edit', Auth::user()->id) }}</i> --}}
                <p>Product Code</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('account.edit', Auth::user()->id) }}">
                {{-- <i class="material-icons">{{ route('account.edit', Auth::user()->id) }}</i> --}}
                <p>Account</p>
            </a>
        </li>
        @endif
        <li class="nav-item ">
            <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST">
                 @csrf
             </form>
        </li>
        {{-- <li class="nav-item ">
        <a class="nav-link" href="{{ route('carrier') }}">
            <i class="material-icons">library_books</i>
            <p>Carrier</p>
        </a>
        </li>
        <li class="nav-item ">
        <a class="nav-link" href="./icons.html">
            <i class="material-icons">bubble_chart</i>
            <p>Icons</p>
        </a>
        </li>
        <li class="nav-item ">
        <a class="nav-link" href="./map.html">
            <i class="material-icons">location_ons</i>
            <p>Maps</p>
        </a>
        </li>
        <li class="nav-item ">
        <a class="nav-link" href="./notifications.html">
            <i class="material-icons">notifications</i>
            <p>Notifications</p>
        </a>
        </li> --}}
        <!-- <li class="nav-item active-pro ">
            <a class="nav-link" href="./upgrade.html">
                <i class="material-icons">unarchive</i>
                <p>Upgrade to PRO</p>
            </a>
        </li> -->
    </ul>
    </div>
</div>