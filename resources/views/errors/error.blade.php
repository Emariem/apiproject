@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h2>SMS Gateway</h2>
                        </div>
                        <div class="card-body  remove-padding-top">
                            <h3>The system encountered an error. Please try after a few minutes or contact the system administrator.</h3>
                            <button type="button" class="btn btn-labeled btn-sm custom-primary-btn" data-toggle="modal" onClick="window.history.back();">
                                    <span class="btn-label" style="color:#fff;">Go Back</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
